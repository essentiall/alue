<?php

// SLIM
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
session_start();
require __DIR__ . '/../vendor/autoload.php';

//CREATING SLIM OBJECT
$app = new \Slim\App
(
[
    "settings" =>
    [ // ACTIVATE DEBUGGING
        "displayErrorDetails" => true
    ],
]
);


$app->add(function (Request $request, Response $response, $next) {
    
    if($request->getMethod() !== 'OPTIONS') {
        return $next($request, $response);
    }
    $response = $response->withHeader('Access-Control-Allow-Methods', $request->getHeaderLine('Access-Control-Request-Method'));
    $response = $response->withHeader('Access-Control-Allow-Headers', $request->getHeaderLine('Access-Control-Request-Headers'));
    $response = $next($request, $response);

    return $response;
});

// REQUIRING DOCUMENTS

require_once __DIR__ . "/../app/constant.php";
require_once __DIR__ . "/../src/coach.schedule.php";
require_once __DIR__ . "/../src/company.php";
require_once __DIR__ . "/../src/filter.php";
require_once __DIR__ . "/../src/test.php";
require_once __DIR__ . "/../src/trainee.php";
require_once __DIR__ . "/../src/lessons.php";
require_once __DIR__ . "/../src/table.php";
require_once __DIR__ . "/../src/authenticate.php";
require_once __DIR__ . "/../src/coach.php";
require_once __DIR__ . "/../src/default.php";
require_once __DIR__ . "/../src/curriculum.php";
require_once __DIR__ . "/../src/summary.php";
require_once __DIR__ . "/../src/schedule.php";
require_once __DIR__ . "/../src/update.php";


$app->run();





