<?php

namespace ALUE\Method;
use \ALUE\Database\Connection as Connection;
use \ALUE\DB\DB as DB;
use \ALUE\Data\Table;

class Handler extends Connection {

    protected $response;
    protected $request;


    // POST METHOD
    public static function post($request, $response, $data, $tablename) {
        $db = new Connection;
            if ($db) {
                foreach($data as $key => $val ) {

                    if ( ! $db->column_check($tablename, $key)) {

                        return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(
                                    array(
                        "response" => array(
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => "could not fetch field ( $key )"
                            )

                            ), 400
                        ); exit;

                    }
                    DB::transaction($tablename, $data, $response);
                    return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(
                                array(
                    "response" => array(
                        "status" => 200,
                        "message" => "OK",
                        "action" => "data inserted successfully"
                        )

                    ), 200
                );

            }
        }
    }

    // PUT METHOD
    public static function put($request, $response, $data, $tablename) {

        $db = new Connection;
         if ($db) {
            $text = filter_var((string)$request->getAttribute('id'), FILTER_SANITIZE_STRING);
             if (is_array($data)) {

                foreach($data as $key => $val ) {
                    if ( ! $db->column_check($tablename, (string)$key) ) {
                        return $response

                        ->withHeader("Content-Type", "application/json")
                        ->withJson(
                                    array(
                        "response" => array(
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => "could not fetch field ( $key )"
                            )

                            ), 400
                        );
                    }
                }
            } else {
                if ( ! $db->column_check($tablename, (string)$data) ) {
                    return $response
                    ->withHeader("Access-Control-Allow-Origin", "*")
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(
                                array(
                    "response" => array(
                        "status" => 400,
                        "message" => "Bad Request",
                        "error" => "could not fetch field ( $data )"
                        )
                        ), 400
                    );
                }
            }

            $check = DB::updater($tablename, $data, $text,$_GET['par'] );

            if ($check) {
                return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(
                array(
                "response" => array(
                    "status" => 200,
                    "message" => "OK",
                    "action" => "record updated succesfully."
                    )

                ), 200
            );
            }


        }
    }
    // DELETE METHOD
    public static function remove($request, $response, $data = NULL, $tablename) {
        $key = $request->getAttribute('id');
        $index = (string)$_GET['par'];
        return DB::deleteRows($tablename, $key, $index, $response);
    }

    public static function method($method = STRING, $request = NULL, $response = NULL, $data = array(), $tablename, $index = NULL) {
       switch($method){
            case "POST": return Handler::post($request, $response, $data, $tablename);
            break;
            case "PUT" : return Handler::put($request, $response, $data, $tablename);
            break;
            case "DELETE" : return Handler::remove($request, $response, $data, $tablename);
            break;
       }
    }

}
