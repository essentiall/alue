<?php

namespace ALUE\INI;


class INI {
    
    public static function get($key = STRING) {
        $config = parse_ini_file('../app/config.ini', true)[ALUE_SERVER][$key];
        return $config;

    }
}