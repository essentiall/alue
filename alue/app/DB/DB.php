<?php

namespace ALUE\DB;
date_default_timezone_set('Asia/Kuala_Lumpur');
use \Firebase\JWT\JWT as JWT;
use \ALUE\Token\Access as Access;
use \ALUE\INI\INI as INI;
use \ALUE\Database\Connection as Connection;
use \ALUE\Error\Error;
use \ALUE\Error\Handler;
use PDO;
use Exception;
class DB extends PDO {

    private static function connect() {
       try {
       // DATABASE Credentials
       $host = INI::get('servername');
       $database = INI::get('database');
       $pass = INI::get('password');
       $user = INI::get('username');
       $charset = INI::get('charset');
       $dsn = "mysql:host=$host;dbname=$database;charset=$charset";

       //PDO Options
       $opt = [
           DB::ATTR_ERRMODE                => DB::ERRMODE_EXCEPTION,
           DB::ATTR_DEFAULT_FETCH_MODE     => DB::FETCH_ASSOC,
           DB::ATTR_EMULATE_PREPARES       => FALSE
       ];

       return new PDO($dsn,$user,$pass,$opt);
       } catch (PDOException $e) {
           return FALSE;
       }
    }

    /* check table if it exists */
    private static function describe($tableName = STRING) {
        if(DB::connect()
            ->query("DESCRIBE `$tableName`")) {
            return DB::connect();
        } else {
            return FALSE;
        }
    }
    public static function tableJoin($table, $d, $row) {


    }
    public static function signedCoaches() {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $curDate = date('Y-m-d', time());

            $stmt = $db->prepare("
            SELECT
            TSD.id AS id,
            CONCAT(TT.last_name, ' ', TT.first_name ) AS trainee_name,
            DATE_FORMAT(TSD.class_date, '%m/%e/%Y') AS class_date,
            TIME_FORMAT(TSD.class_start_time, '%k:%i') AS start_time,
            TL.lesson_name AS lesson_name,
            CONCAT(TC.last_name, ' ', TC.first_name ) AS coach_name

            FROM tbl_trainee_schedule_detail TSD

            LEFT JOIN tbl_coach TC
            ON TSD.coach_id = TC.id

            LEFT JOIN tbl_trainee_schedule_header TH
            ON TSD.trainee_schedule_id = TH.id

            LEFT JOIN tbl_trainee TT
            ON TT.trainee_id = TH.trainee_id

            LEFT JOIN tbl_lessons TL
            ON TSD.lesson_id = TL.id

            WHERE TSD.coach_id > 0 && TSD.coach_id != 0 && TSD.class_date >= ?
            ");
            $stmt->execute([$curDate]);
            while($rows = $stmt->fetchAll()){
                foreach($rows as $key => $value) {
                    foreach($rows[$key] as $k => $j ) {
                        if ($k === 'id') {
                            $id = $j;
                            continue;
                        }
                        if ($k === 'trainee_name') {
                            $trainee = $j;
                            continue;
                        }
                        if ($k === 'class_date') {
                            $classDate = date('Y-m-d H:i:s', strtotime("$j"));

                            continue;
                        }
                        if ($k === 'start_time') {
                            $startTime = date('Y-m-d H:i:s', strtotime("$j"));
                            continue;
                        }
                        if ($k === 'lesson_name') {
                            $lesson = $j;
                            continue;
                        }
                        if ($k === 'coach_name') {
                            $coach = $j;
                            continue;
                        }


                    } $process = $db->prepare("INSERT INTO tmp_assigned_trainee_schedule_detail ( id, trainee_name, class_date, start_time, lesson_name, coach_name ) VALUES (?, ?, ?, ?, ?, ?)");
                    $process->execute([
                        $id,
                        $trainee,
                        $classDate,
                        $startTime,
                        $lesson,
                        $coach
                    ]);
                } $data = $rows;
            }
            $db->commit();
            return isset($data) ? $data : false;
        } catch(DBException $e){
            $db->rollBack();
        }
    }
    public static function lessonHistory( STRING $UNIQUE_ID = NULL ) {
        $db = DB::connect();
        $db->beginTransaction();
        $curDate =date('Y-m-d', strtotime('-1 day'));
        $curTime = date('H:i:s', TIME() - 3600);
        try {
            $table = (STRING)("_" . $UNIQUE_ID);
            $STMT = $db->query("CREATE TABLE IF NOT EXISTS $table
            ( 
                id INT(11) PRIMARY KEY,
                trainee_name VARCHAR(50),
                class_date DATE,
                start_time TIME,
                lesson_name VARCHAR(100),
                coach_name VARCHAR(50),
                remarks VARCHAR(100) NOT NULL DEFAULT '\"\"'
            ) CHARACTER SET utf8 COLLATE utf8_general_ci");
            $STMT = NULL;

            $STMT = $db->prepare("SELECT 
            TSD.id AS id,
            IFNULL(CONCAT(TT.last_name, ' ', TT.first_name), '') AS trainee_name,
            DATE_FORMAT(TSD.class_date, '%m/%e/%Y') AS class_date,
            TIME_FORMAT(TSD.class_start_time, '%k:%i') AS start_time,
            TL.lesson_name AS lesson_name,
            IFNULL(CONCAT(TC.last_name, ' ', TC.first_name ), '') AS coach_name

            FROM tbl_trainee_schedule_detail TSD

            CROSS JOIN tbl_coach TC
            ON TSD.coach_id = TC.id

            CROSS JOIN tbl_trainee_schedule_header TH
            ON TSD.trainee_schedule_id = TH.id

            CROSS JOIN tbl_trainee TT
            ON TT.trainee_id = TH.trainee_id

            CROSS JOIN tbl_lessons TL
            ON TSD.lesson_id = TL.id
            WHERE  DATE(?) < CURDATE() AND TIME(?) < CURTIME()
            ");
            $STMT->execute([$curDate, $curTime]);
            while ( $rows = $STMT->fetchAll()) {
                $COLLECTION = $rows;
            } $STMT = NULL;
            if (isset($COLLECTION)) {
                foreach($COLLECTION as $key => $value) {
                    foreach($COLLECTION[$key] as $k => $j ) {
                        if ($k === 'id') {
                            $id = $j;
                            continue;
                        }
                        if ($k === 'trainee_name') {
                            $trainee = $j;
                            continue;
                        }
                        if ($k === 'class_date') {
                            $classDate = date('Y-m-d H:i:s', strtotime("$j"));

                            continue;
                        }
                        if ($k === 'start_time') {
                            $startTime = date('Y-m-d H:i:s', strtotime("$j"));
                            continue;
                        }
                        if ($k === 'lesson_name') {
                            $lesson = $j;
                            continue;
                        }
                        if ($k === 'coach_name') {
                            $coach = $j;
                            continue;
                        }
                    
                    }
                    $STMT = $db->prepare("INSERT INTO `$table` ( id, trainee_name, class_date, start_time, lesson_name, coach_name ) VALUES (?, ?, ?, ?, ?, ?)");
                    $STMT->execute([
                        $id,
                        $trainee,
                        $classDate,
                        $startTime,
                        $lesson,
                        $coach
                    ]);
                }
            }
            if (isset($COLLECTION)) {
                $db->commit();
                return true;
            }
            return false;


            
        } catch(PDOException $e) {
            $db->rollBack();

        }
       







    }

    public static function unsignedCoaches() {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $curDate = date('Y-m-d', time());

            $stmt = $db->prepare("
            SELECT
            TSD.id AS id,
            CONCAT(TT.last_name, ' ', TT.first_name ) AS trainee_name,
            DATE_FORMAT(TSD.class_date, '%m/%e/%Y') AS class_date,
            TIME_FORMAT(TSD.class_start_time, '%k:%i') AS start_time,
            TL.lesson_name AS lesson_name,
            CONCAT(TC.last_name, ' ', TC.first_name ) AS coach_name

            FROM tbl_trainee_schedule_detail TSD

            LEFT JOIN tbl_coach TC
            ON TSD.coach_id = TC.id

            LEFT JOIN tbl_trainee_schedule_header TH
            ON TSD.trainee_schedule_id = TH.id

            LEFT JOIN tbl_trainee TT
            ON TT.trainee_id = TH.trainee_id

            LEFT JOIN tbl_lessons TL
            ON TSD.lesson_id = TL.id

            WHERE TSD.coach_id = 0 && TSD.class_date >= ? && TL.coach_assigned_flg = 1
            ");
            $stmt->execute([$curDate]);
            while($rows = $stmt->fetchAll()){
                foreach($rows as $key => $value) {
                    foreach($rows[$key] as $k => $j ) {
                        if ($k === 'id') {
                            $id = $j;
                            continue;
                        }
                        if ($k === 'trainee_name') {
                            $trainee = $j;
                            continue;
                        }
                        if ($k === 'class_date') {
                            $classDate = date('Y-m-d H:i:s', strtotime("$j"));

                            continue;
                        }
                        if ($k === 'start_time') {
                            $startTime = date('Y-m-d H:i:s', strtotime("$j"));
                            continue;
                        }
                        if ($k === 'lesson_name') {
                            $lesson = $j;
                            continue;
                        }
                        if ($k === 'coach_name') {
                            $coach = $j;
                            continue;
                        }


                    } $process = $db->prepare("INSERT INTO tmp_unassigned_trainee_schedule_detail ( id, trainee_name, class_date, start_time, lesson_name, coach_name ) VALUES (?, ?, ?, ?, ?, ?)");
                    $process->execute([
                        $id,
                        $trainee,
                        $classDate,
                        $startTime,
                        $lesson,
                        $coach
                    ]);
                } $data = $rows;
            }
            $db->commit();
            return isset($data) ? $data : false;
        } catch(DBException $e){
            $db->rollBack();
        }
    }
    public static function updater($tableName = STRING, $data = array(), $index = STRING,$keys = STRING) {
        $db = DB::connect();
        $db->beginTransaction();
        try {


            if (is_array($data)) {
                foreach($data as $key => $val) {
                   $stmt =  $db->prepare("UPDATE $tableName SET $key = ? WHERE $keys LIKE ? OR $keys = ?");
                   $stmt->execute([$val,$index,$index]);
                }
            } else {
                $stmt = $db->query("UPDATE $tableName SET $keys = '$data' WHERE $keys LIKE ? OR $keys = ?");
                $stmt->execute([$data,$index,$index]);
            }
            $db->commit();
            return true;

        } catch(Exception $e) {

            $db->rollBack();
            return false;
        }



    }
    public static function addTraineeSchedule($table, $data = array(), $response) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $detailCount = count($data['detail']);
            $counter = 0;
            $fields = '';
            foreach($data['detail'] as $value) {
               foreach ($value as $k => $i ){
                    if ($k === 'trainee_schedule_id') {
                        $TSI = 'trainee_schedule_id';
                        $TSIvalue = $i;
                        continue;
                    }
                    if ($k === 'coach_id') {
                        $CI = 'coach_id';
                        $CIvalue = $i;
                        continue;
                    }
                    if ($k === 'lesson_id') {
                        $LI = 'lesson_id';
                        $LIvalue = $i;
                        continue;
                    }
                    if ($k === 'class_start_date') {
                        $SD = 'class_date';
                        $SDvalue = date('Y-m-d H:i:s',strtotime($i));
                        continue;
                    }
                    if ($k === 'class_start_time') {
                        $ST = 'class_start_time';
                        $STvalue = date('Y-m-d H:i:s',strtotime($i));
                        continue;
                    }
                    if ($k === 'class_end_time') {
                        $ET = 'class_end_time';
                        $ETvalue = date('Y-m-d H:i:s',strtotime($i));
                        continue;
                    }
               }
               $stmt = $db->prepare("INSERT INTO $table ($TSI, $CI, $LI, $SD, $ST, $ET ) VALUES (?, ?, ?, ?, ?, ? )");
               $stmt->execute([
                $TSIvalue,
                $CIvalue,
                $LIvalue,
                $SDvalue,
                $STvalue,
                $ETvalue
               ]);
            }
            $db->commit();
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(
            array(
            "response" => array(
                "status" => 200,
                "message" => "OK",
                "detail" => "Trainee schedule added successfully."
            )
        ), 200
        );
        } catch(DBException $e) {
            $db-rollBack();
        }
    }
    public static function addCurriculum($tableName = STRING, $data = array()) {
        if ($db = DB::connect()) {
            try {

                $db->beginTransaction();
                $df = $data['default_time_lessons'];
                $detail = $data['detail'];
                $count = count($data);

                $counter =  0;
                $cHeader = '';
                $cHeaderValue = '';
                $defaultTimeHeader = '';
                    foreach($data as $key => $value) {
                        $counter++;
                        if ($key === 'curriculum_id') {
                            $curriculum_id =  $value;
                        }
                        if ($counter === 4) {
                            $cHeader .= "`$key` )";
                            $cHeaderValue .= "'$value' )";
                            // CURRICULUM HEADER
                            $cHeader = "( $cHeader ";
                            $cHeaderValue = "( $cHeaderValue ";
                            $cHead = $cHeader . ' VALUES ' . $cHeaderValue;
                            $stmt = $db->exec("INSERT INTO tbl_curriculum_set_header $cHead");
                            $stmt = NULL;
                            foreach($df as $key) {
                                foreach($key as $k => $j) {
                                    $stmt = $db->prepare("INSERT INTO tbl_curriculum_set_default_time_lessons (`$k`, `curriculum_id`) VALUES ( ?, ? )");
                                    $stmt->execute([$j, $curriculum_id]);
                                }
                            }
                            foreach($detail as $i) {
                                foreach($i as $x => $y) {
                                    if ($x === 'lesson_id') {
                                        $lessonId = $x;
                                        $lessonIdValue = $y;
                                        continue;
                                    }
                                    if ($x === 'week_num') {
                                        $weekNum = $x;
                                        $weekNumValue = $y;
                                        continue;
                                    }
                                    if ($x === 'day_num') {
                                        $dayNum = $x;
                                        $dayNumValue = $y;
                                        continue;
                                    }
                                    if ($x === 'sort_num') {
                                        $sortNum = $x;
                                        $sortNumValue = $y;
                                        continue;
                                    }

                                }
                                $stmt1 = $db->prepare("INSERT INTO tbl_curriculum_set_detail (  `curriculum_id`, `$lessonId`, `$weekNum`, `$dayNum`, `$sortNum` ) VALUES ( ?, ?, ?, ?, ? )");
                                    $stmt1->execute([
                                        $curriculum_id,
                                        $lessonIdValue,
                                        $weekNumValue,
                                        $dayNumValue,
                                        $sortNumValue
                                ]);
                            }
                            break;
                        }
                        $cHeader .= "`$key`, ";
                        $cHeaderValue .= "'$value', ";
                    }
                $db->commit();
                return true;
            } catch(DBException $e) {
                $db->rollBack();
                return false;


            }

        }
    }
    public static function getTraineeDetails($table = STRING, $key, $index, $response){

        $db = DB::connect();
        $db->beginTransaction();
        try {
            $dat = array();
            $query = "
            SELECT
            TSH.id AS id,
            TCSH.curriculum_name AS curriculum_name,
            CONCAT( TT.last_name, ' ', TT.first_name ) AS trainee_name,
            TSH.generated_flg AS generated_flg

            FROM `$table` TSH
            LEFT JOIN `tbl_curriculum_set_header` TCSH
            ON TSH.curriculum_id = TCSH.curriculum_id

            LEFT JOIN tbl_trainee TT
            ON TSH.trainee_id = TT.trainee_id

            WHERE TSH.trainee_id = ?";

            $query1 = "
            SELECT
            TSD.id AS id,
            TSD.trainee_schedule_id AS trainee_schedule_id,
            TSD.coach_id AS coach_id,
            TSD.lesson_id AS lesson_id,
            TL.lesson_name AS lesson_name,
            DATE_FORMAT( TSD.class_date, '%m/%e/%Y' ) AS class_start_date,
            TIME_FORMAT(TSD.class_start_time, '%k:%i') AS class_start_time,
            TIME_FORMAT(TSD.class_end_time, '%k:%i') AS class_end_time

            FROM $table TSH
            LEFT JOIN tbl_trainee_schedule_detail TSD
            ON TSH.id = TSD.id
            LEFT JOIN tbl_lessons TL
            ON TSD.lesson_id = TL.id

            WHERE TSH.id = ?";

            $stmt = $db->prepare($query);
            $stmt->execute([ $index ]);
            $stmt1 = $db->prepare($query1);
            $stmt1->execute([ $index ]);
            while($rows = $stmt->fetch(DB::FETCH_ASSOC)) {
                $data = $rows;
                while($rows2 = $stmt1->fetch(DB::FETCH_ASSOC)) {
                    $data['detail'] = array($rows2);
                }
            }
            $db->commit();
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson( array("response" => $data), 200);


        } catch(DBException $e) {

            $db-rollBack();
        }




    }
    public static function getDetails($table, $key, $index, $response ) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $stmt = $db->prepare("SELECT * FROM $table WHERE `$key` = ?");
            $stmt->execute([$index]);
            while($rows = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                $id = $rows[0]['id'];
                $cid = $id = $rows[0]['curriculum_id'];
                $tid = $id = $rows[0]['trainee_id'];
                $stmt1 = $db->prepare("SELECT curriculum_name FROM tbl_curriculum_set_header WHERE curriculum_id = ?");
                $stmt1->execute([$cid]);
                while($rows1 = $stmt1->fetch()) {
                    $stmt2 = $db->prepare("SELECT last_name, first_name FROM tbl_trainee WHERE trainee_id = ?");
                    $stmt2->execute([$tid]);
                    while($rows2 = $stmt2->fetch()){
                        $data = array();
                        $data['id'] = $id;
                        $data['curriculum_name'] = $rows1['curriculum_name'];
                        $data['trainee_name'] = $rows2['last_name'] . " " . $rows2['first_name'];
                        $data['generated_flg'] = $rows[0]['generated_flg'];
                        $stmt3 = $db->prepare("SELECT * FROM tbl_trainee_schedule_detail WHERE id = ?");
                        $stmt3->execute([$id]);
                        while($rows3 = $stmt3->fetchAll(DB::FETCH_ASSOC)) {
                            $lessonId = $rows3[0]['lesson_id'];
                            $stmt4 = $db->prepare("SELECT lesson_name FROM tbl_lessons WHERE id = ?");
                            $stmt4->execute([$lessonId]);
                            while($rows4 = $stmt4->fetch()){
                                $data['detail'][0]['id'] = $id;
                                $data['detail'][0]['trainee_schedule_id'] = $rows3[0]['trainee_schedule_id'];
                                $data['detail'][0]['coach_id'] = $rows3[0]['coach_id'];
                                $data['detail'][0]['lesson_id'] = $lessonId;
                                $data['detail'][0]['lesson_name'] = $rows4['lesson_name'];
                                $data['detail'][0]['class_start_date'] = date('m/j/Y', strtotime($rows3[0]['class_date']));
                                $data['detail'][0]['class_start_time'] = date('G:i', strtotime($rows3[0]['class_start_time']));
                                $data['detail'][0]['class_end_time'] = date('G:i', strtotime($rows3[0]['class_end_time']));
                            }
                        }
                    }
                }
            }
            $db->commit();
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson( array("response" => $data), 200);
        }
        catch(DBException $e) {


        }
    }
    public static function cell($table, $key, $index) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $stmt = $db->prepare("SELECT * FROM $table WHERE `$key` = ?");
            $stmt->execute([$index]);
            while($rows = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                $row = $rows;
            }

            $db->commit();
            return $row;

        } catch(DBException $e) {

            $db->rollBack();
        }

    }
    public static function getTraineeDetail($tableName = STRING, $response, $par, $index ) {
        if ($db = DB::connect()) {
            try {
                $db->beginTransaction();

                $tHeader = $db->prepare("SELECT * FROM `$tableName` WHERE `$par` = ?");
                $tHeader->execute([$index]);

                while($row = $tHeader->fetchAll(DB::FETCH_ASSOC)) {
                    $tDetail = $db->prepare("SELECT * FROM `tbl_curriculum_set_header` WHERE curriculum_id = ?");
                    $tDetail->execute([$row[0]['curriculum_id']]);
                    while($row1 = $tDetail->fetchAll(DB::FETCH_ASSOC)) {
                        $trainee = $db->prepare("SELECT * FROM `tbl_trainee` WHERE trainee_id = ?");
                        $trainee->execute([$row[0]['trainee_id']]);
                        while($row2 = $trainee->fetchAll(DB::FETCH_ASSOC)){
                            $traineeDetail = $db->prepare("SELECT * FROM `tbl_trainee_schedule_detail` WHERE id = ?");
                            $traineeDetail->execute([$row[0]['id']]);
                            while($row3 = $traineeDetail->fetchAll(DB::FETCH_ASSOC)){
                                $td = $row3;
                                $data = array("response" => array());
                                $data['response'][0]['id'] = $row[0]['id'];
                                $data['response'][0]['curriculun_name'] = $row1[0]['curriculum_name'];
                                $data['response'][0]['trainee_name'] = $row2[0]['last_name'] . ' ' . $row2[0]['first_name'];
                                $data['response'][0]['generated_flg'] = $row[0]['generated_flg'];
                                $mDetails = function($database, $id, $td ) {
                                    $stmt = $database->prepare("SELECT * FROM `tbl_curriculum_set_detail` WHERE `curriculum_id` = ?");
                                    $stmt->execute([$id]);
                                    while($rows = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                                        $data = array();
                                        foreach($rows as $key => $val) {
                                        $lesson = $database->prepare("SELECT * FROM tbl_lessons WHERE id = ?");
                                        $lesson->execute([$val['lesson_id']]);
                                            while ($lessons = $lesson->fetchAll(PDO::FETCH_ASSOC)) {
                                                array_push($data,
                                                    array(
                                                        "id" => $val['id'],
                                                        "lesson_name" => $lessons[0]['lesson_name'],
                                                        "trianee_schedule_id" => $td[0]['trainee_schedule_id']
                                                    )
                                                );
                                            }
                                        }

                                    }

                                    return $data;

                                };
                                $data['response'][0]['detail'] = $mDetails($db, $row[0]['curriculum_id'], $td);
                            }
                        }
                    }
                }
                   // print_r($d[1]['id']);
                   // exit;
                $db->commit();
                return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(array("response" => $data['response'][0] ), 200 );
            } catch(DBException $e) {
                $db->rollBack();
            }
        }
    }
    public static function getCurriculumSet($table, $response, $par, $index) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $header = $db->prepare("SELECT
            TCSH.curriculum_id AS curriculum_id,
            TCSH.curriculum_name AS curriculum_name,
            TCSD.week_num AS total_weeks_num,
            TCSH.assesstment_lvl AS assesstment_lvl

            FROM $table TCSH

            LEFT JOIN tbl_curriculum_set_detail TCSD
            ON TCSH.curriculum_id = TCSD.curriculum_id


            WHERE TCSH.$par = ?
            ");
            $header->execute([$index]);

            $defaultTimeLesson = $db->prepare("SELECT

            DT.id AS id,
            DT.lesson_id AS lesson_id,
            TL.lesson_name AS lesson_name

            FROM tbl_curriculum_set_default_time_lessons DT

            LEFT JOIN tbl_lessons TL
            ON DT.lesson_id = TL.id

            WHERE DT.curriculum_id = ?
            ");
            $defaultTimeLesson->execute([$index]);
            while($rows = $header->fetchAll(DB::FETCH_ASSOC)) {
                $data = $rows;
                while($rows1 = $defaultTimeLesson->fetchAll(DB::FETCH_ASSOC)) {
                    $data['default_time_lessons'] = $rows1;
                }
            }


            $db->commit();
            return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(array( "response" => $data), 200);
        } catch(DBException $e) {

            $db->rollBack();
        }

    }
    public static function getRows($tableName = STRING, $response, $index = NULL,  $par = NULL ) {
        if($db = DB::connect()) {
            try {
                $db->beginTransaction();

                if ($par) {
                    $stmt = $db->prepare("SELECT * FROM $tableName WHERE $par = ?");
                    $stmt->execute([$index]);
                    $stmt1 = $db->prepare("SELECT * FROM tbl_curriculum_set_default_time_lessons WHERE $par = ?");
                    $stmt1->execute([$index]);
                    $stmt2 = $db->prepare("SELECT * FROM tbl_curriculum_set_detail WHERE $par = ?");
                    $stmt2->execute([$index]);
                    while ($rows = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                        while($rows1 = $stmt1->fetchAll(DB::FETCH_ASSOC) ) {
                            $data1 = $rows1;
                            while($rows2 = $stmt2->fetchAll(DB::FETCH_ASSOC) ) {
                                $data2 = $rows2;
                                $data = array("response" => $rows );
                                $data['response'][0]['default_time_lessons'] = $data1;
                                $data['response'][0]['detail'] = $data2;
                            }
                        }
                    }
                    $db->commit();
                    return $response->withHeader("Content-Type", "application/json")
                                    ->withJson(array( "response" => $data['response'][0]), 200);
                } else {
                    $stmt = $db->prepare("SELECT * FROM $tableName");
                    $stmt->execute();
                    while ($rows = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                        $data = $rows;
                    }
                    $db->commit();
                    return $data;
                }
            }
            catch (PDOException $e) {
                $bd->rollBack();
                return $response->withHeader('Content-Type', 'application/jso')
                                ->withJson(array(
                                    "response" => 400,
                                    "message" => "Bad Request",
                                    "error" => $e->getMessage(),
                ), 400);
            }

        }
    }

    public static function row($tableName = STRING, $key = STRING, $index = STRING) {
        if($db = DB::connect()) {
            try{
            $db->beginTransaction();
            $stmt = $db->prepare("SELECT * FROM $tableName WHERE $key = ?");
            $stmt->execute([$index]);
                while ($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {

                    $data = $row;
                }
            $db->commit();
            return $data;
            } catch(PDOException $e) {

                $db->rollBack();
            }
        }
    }
    public static function filter($tableName = STRING, $query = STRING, $keys = STRING, $response){
        if($db = DB::connect()){
            try {
                $db->beginTransaction();
                $stmt = $db->prepare("SELECT * FROM $tableName WHERE $query = ?");
                $stmt->execute([$keys]);
                while ($rows = $stmt->fetchAll(PDO::FETCH_ASSOC)) {

                    $data = $rows;
                }

                $db->commit();

               return $data;
            } catch (PDOException $e) {
                $db->rollBack();
                return false;
            }
        }
    }
    public static function removeTable(STRING $table) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $db->exec("DROP TABLE IF EXISTS `$table`");
            $db->commit();
        } catch (PDOException $e) {

            $db->rollBack();
        }

    }
    public static function deletTableRows($tableName) {
        if($db= DB::connect()) {
            try {
                $db->beginTransaction();
                $stmt = $db->exec("DELETE FROM $tableName");
                $db->commit();
            } catch(DBException $e) {

                $db->rollBack();
            }

        }

    }
    public static function insertRecord($tableName = STRING, $data = array(), $K = STRING, $I = STRING) {
        $fields = "";
        $keys = "";
        $count = count($data);
        $counter = 0;
        foreach($data as $key => $value) {
            $counter++;

            if($counter === $count) {
                $fields .= "`$key` ";
                $keys .= "'$value' ";

                continue;
            }
            if($key === 'schedule_date') {
                $d1 = date('Y-m-d H:i:s', strtotime($value));
                $keys .= "'$d1', ";
                $fields .= "`$key`, ";
                continue;
            }
            if($key === 'schedule_start_time') {
                $d1 = date('Y-m-d H:i:s', strtotime($value));
                $keys .= "'$d1', ";
                $fields .= "`$key`, ";
                continue;
            }
            if($key === 'schedule_end_time') {
                $d1 = date('Y-m-d H:i:s', strtotime($value));
                $keys .= "'$d1', ";
                $fields .= "`$key`, ";
                continue;
            }


            $fields .= "`$key`, ";
            $keys .= "'$value' ,";
        }
        $Field = "( $fields )";
        $Key = "( $keys )";
        $combine = $Field .  ' VALUES ' . $Key;
        //return $combine;
        if($db = DB::connect()){
            try {
            $db->beginTransaction();

            $stmt = $db->exec("INSERT INTO `$tableName` $combine");


            $db->commit();
            return true;
            } catch(PDOException $e) {
                $db->rollBack();
            }
        }
    }
    public static function transaction($tableName = STRING, $data = array(), $response) {
        if($db = DB::connect()) {
            try {
                $db->beginTransaction();
                $fields = "";
                $fieldsValue = "";
                $count = count($data);
                $counter = 0;
                foreach($data as $key => $val){
                    $counter += 1;
                    if ($counter == $count) {
                        $fields .= "$key ";
                        $fieldsValue .= "'$val' ";
                        continue;
                    }
                    if (gettype($val) == 'integer') {
                        $fieldsValue .= "$val, ";
                        continue;
                    }
                    $fields .= "$key, ";
                    $fieldsValue .= "'$val', ";
                } $queryString = "INSERT IGNORE INTO `$tableName` (" . $fields . ") VALUES(" . $fieldsValue . ")";

                $stmt = $db->exec($queryString);
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                return $response->withHeader('Content-Type', 'application/json')
                                ->withJson(array(
                                    "response" => 400,
                                    "message" => "Bad Request",
                                    "error" => $e->getMessage(),

                ), 400);
            }
        }
    }
    // Get single row
    public static function getRow($tableName = STRING, $index = STRING, $queryString = NULL ) {
        if( $db = DB::connect() ) {
            $stmt = $db->prepare("SELECT * FROM `$tableName` WHERE `$index` = ? ");
            $stmt->execute
            (

                [
                    $queryString
                ]
            );
            $row = $stmt->fetch(DB::FETCH_ASSOC);
            return $row;
        }
    }

    // Get all rows
    public static function getAllRows($tableName = STRING, $response, $dateFormat = NULL) {
        if ($db = DB::connect()){
            // Connection establish
            if ( $dateFormat !== NULL ) {
               if ($dateFormat === 'tbl_coach_schedule') {
                $stmt = $db->query("SELECT *,
                DATE_FORMAT(schedule_date, '%m/%e/%Y') AS 'schedule_date',
                DATE_FORMAT(schedule_start_time, '%k:%i') AS 'schedule_start_time',
                DATE_FORMAT(schedule_end_time, '%k:%i') AS 'schedule_end_time'

                FROM
                tbl_coach_schedule");
                while ($rows = $stmt->fetchAll()) {
                    $data = array( "data" => $rows );
                }
                return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson($data, 200);
                        exit;
               }
            }
            if ( $dateFormat !== NULL ) {
                if ($dateFormat === 'tbl_trainee') {
                 $stmt = $db->query("SELECT *,

                 DATE_FORMAT(study_period_start_date, '%m/%e/%Y') AS 'study_period_start_date',
                 DATE_FORMAT(study_period_end_date, '%m/%e/%Y') AS 'study_period_end_date'
                 FROM  tbl_trainee");
                 while ($rows = $stmt->fetchAll()) {
                     $data = array( "data" => $rows );
                 }
                 return $response
                         ->withHeader("Content-Type", "application/json")
                         ->withJson($data, 200);
                         exit;
                }
             }
            $stmt = $db->query("SELECT * FROM $tableName");
            while ($rows = $stmt->fetchAll()) {
                $data = array( "data" => $rows );

            }
            return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson($data, 200);
        }

    }
    public static function summary($table, $get, $response, $param ) {
        $db = DB::connect();
        $db->beginTransaction();

        try {
          if ($param) {
            $raw = explode(',', $param );
            if (count($raw) > 1) {
              list($dat, $id) = $raw;
              list($M1, $D1, $Y1) = explode("-", $dat);
              $date = date('Y-m-d H:i:s', mktime(0,0,0,$M1,$D1,$Y1));
            $stmt = $db->prepare("SELECT
            TS.trainee_schedule_id AS id,
            TH.id AS id,
            TS.id AS detail_id,
            TH.curriculum_id as curriculum_id,
            CONCAT(TT.last_name, ' ', TT.first_name ) AS trainee_name,
            TH.trainee_id AS trainee_id,
            IFNULL(CONCAT(TC.last_name, ' ', TC.first_name), '') AS coach_name,
            IFNULL(TT.coach_id_incharge, '') AS coach_id_incharge,
            TS.coach_id AS coach_id,
            TL.id AS lesson_id,
            TL.lesson_name AS lesson_name,
            DATE_FORMAT(TS.class_date, '%m/%e/%Y') AS class_date,
            TIME_FORMAT(TS.class_start_time, '%H:%i') AS class_start_time,
            TIME_FORMAT(TS.class_end_time, '%H:%i') AS class_end_time
            FROM
            tbl_trainee_schedule_detail TS STRAIGHT_JOIN
            tbl_trainee_schedule_header TH STRAIGHT_JOIN
            tbl_trainee TT STRAIGHT_JOIN
            tbl_lessons TL
            LEFT JOIN tbl_coach TC
            ON TC.id = TS.coach_id

            WHERE TS.class_date = DATE(?)
            AND TT.company_id = ?
            AND TS.trainee_schedule_id = TH.id
            AND TH.trainee_id = TT.trainee_id
            AND TS.lesson_id = TL.id");
            $stmt->execute([$date, $id]);
            while( $rows = $stmt->fetchAll(DB::FETCH_ASSOC)){

                $data = $rows;
            }
            } else {
              list($M1, $D1, $Y1) = explode("-", $param);
              $date = date('Y-m-d H:i:s', mktime(0,0,0,$M1,$D1,$Y1));
            $stmt = $db->prepare("SELECT
            TS.trainee_schedule_id AS id,
            TH.id AS id,
            TS.id AS detail_id,
            TH.curriculum_id as curriculum_id,
            CONCAT(TT.last_name, ' ', TT.first_name ) AS trainee_name,
            TH.trainee_id AS trainee_id,
            IFNULL(CONCAT(TC.last_name, ' ', TC.first_name), '') AS coach_name,
            IFNULL(TT.coach_id_incharge, '') AS coach_id_incharge,
            TS.coach_id AS coach_id,
            TL.id AS lesson_id,
            TL.lesson_name AS lesson_name,
            DATE_FORMAT(TS.class_date, '%m/%e/%Y') AS class_date,
            TIME_FORMAT(TS.class_start_time, '%H:%i') AS class_start_time,
            TIME_FORMAT(TS.class_end_time, '%H:%i') AS class_end_time
            FROM
            tbl_trainee_schedule_detail TS STRAIGHT_JOIN
            tbl_trainee_schedule_header TH STRAIGHT_JOIN
            tbl_trainee TT STRAIGHT_JOIN
            tbl_lessons TL
            LEFT JOIN tbl_coach TC
            ON TC.id = TS.coach_id

            WHERE TS.class_date = DATE(?)
            AND TS.trainee_schedule_id = TH.id
            AND TH.trainee_id = TT.trainee_id
            AND TS.lesson_id = TL.id");
            $stmt->execute([$date]);
            while( $rows = $stmt->fetchAll(DB::FETCH_ASSOC)){

                $data = $rows;
            }
          }

          } else {
            if (count($get) > 2) {
                throw new Exception("only two parameter's are allowed");
            } else {

                foreach ( $get as $key => $val ) {
                    $stmt = $db->prepare("SELECT
                    T.id AS id,
                    T.curriculum_id AS curriculum_id,
                    C.curriculum_name AS curriculum_name,
                    CONCAT( TT.last_name, ' ', TT.first_name ) AS trainee_name,
                    T.trainee_id AS trainee_id,
                    TS.id AS detail_id,
                    TS.coach_id AS coach_id,
                    CONCAT(TC.last_name, ' ', TC.first_name ) AS coach_name,
                    TS.lesson_id AS lesson_id,
                    TL.lesson_name AS lesson_name,
                    DATE_FORMAT(TS.class_date, '%m/%e/%Y') AS class_date,

                    TIME_FORMAT(TS.class_start_time, '%k:%i') AS class_start_time,
                    TIME_FORMAT(TS.class_end_time, '%k:%i') AS class_end_time

                    FROM $table T

                    LEFT JOIN tbl_curriculum_set_header C
                    ON T.curriculum_id = C.curriculum_id

                    LEFT JOIN tbl_trainee TT
                    ON T.trainee_id = TT.trainee_id

                    LEFT JOIN tbl_trainee_schedule_detail TS
                    ON T.id = TS.trainee_schedule_id

                    LEFT JOIN tbl_coach TC
                    ON TS.coach_id = TC.id

                    LEFT JOIN tbl_lessons TL
                    ON TS.lesson_id = TL.id


                    WHERE T.id = ? || T.id = ?");
                    $stmt->execute([$key,$val]);
                    while( $rows = $stmt->fetchAll(DB::FETCH_ASSOC)){

                        $data = $rows;
                    }
                }

            }
          }
          if (isset($data)) {
              return $response
              ->withHeader("Content-Type", "application/json")
              ->withJson(array( "response" => $data
              ), 200);
          } else {
            $db->commit();
              return $response
              ->withHeader("Content-Type", "application/json")
              ->withJson(array(
                  "status" => 400,
                  "message" => "Bad Request",
                  "error" => "unable to query table[$table] please check you parameter's"
              ), 200);
          }

        } catch(Exception $e) {
            $db->rollBack();
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(array(
                "status" => 400,
                "message" => "Bad Request",
                "error" => $e->getMessage()
            ), 400);
        }
    }
    public static function update($tableName = STRING, $data = array(), $key = STRING, $index = STRING) {
        if ($db = DB::connect()){
            $num = count($data);
            if( $num > 0 ){

                foreach($data as $keys => $value ){
                    $stmt = $db
                            ->prepare("UPDATE `$tableName` SET `$keys` = ? WHERE `$key` = '$index'")
                            ->execute([ $value ]);

                }
            }
        }
    }
    public static function login($data =  array(), $username = NULL, $password = NULL, $response ) {
        if ($db = DB::connect()) {
            foreach($data as $keys ) {
                if ($keys === 'admin') {
                    $stmt = $db->query("SELECT `username`, `password` FROM `$keys` WHERE `username` = '$username'");
                    if ($user = $stmt->fetch()) {
                       if (password_verify($password, $user['password'])) {
                           $iat = time();
                           $iss = gethostname();
                           $uid = (string)$user['username'];
                           $notBefore = $iat + 10;
                           $exp = $notBefore + (15000 * 60);
                           $payload = array(
                               "iat" => $iat,
                               "iss" => $iss,
                               "exp" => $exp,
                               "uid" => $uid
                           );

                           return $response
                                    ->withJson([
                                        "response" => [
                                            "status" => 200,
                                            "message" => "OK",
                                            "data" => [
                                                "acc" => "administrator",
                                                "uid" => $user['username']."@administrator",
                                                "oauth" => JWT::encode($payload, ALUE_KEY),
                                        ]
                                    ]], 200);
                       } else {
                           return $response
                                    ->withHeader("Content-Type", "application/json")
                                    ->withJson([
                                        "response" => array
                                        (
                                            "status" => 402,
                                            "message" => "Unauthorized",
                                            "error" => "ADMIN: invalid password"
                                        )
                                    ], 402);
                       }
                    }
                }
                if ($keys === 'tbl_coach') {
                    $stmt = $db->query("SELECT `email`, `password`, `first_name` FROM `$keys` WHERE `email` = '$username'");
                    if ( $user = $stmt->fetch()) {
                        if(password_verify($password, $user['password'])){
                            DB::update('tbl_coach', array("offline_coach_flg" => 1), 'email', $user['email']);
                            return $response
                                    ->withHeader("Content-Type", "application/json")
                                    ->withJson([
                                        "response" => [
                                            "status" => 200,
                                            "message" => "OK",
                                            "data" => [ "account_type" => "coach", "username" => $user['email']
                                        ]
                                    ]], 200);
                        } else {
                            return $response
                                     ->withHeader("Content-Type", "application/json")
                                     ->withJson([
                                         "response" => array
                                         (
                                             "status" => 402,
                                             "message" => "Unauthorized",
                                             "error" => "COACH: invalid password"
                                         )
                                     ], 402);
                        }
                    }

                }
                if ($keys === 'tbl_trainee') {

                    $stmt = $db->query("SELECT `trainee_id` FROM `$keys` WHERE `trainee_id` = '$username'");

                    if ( $user = $stmt->fetch()) {
                        return $response
                                    ->withHeader("content-Type", "application/json")
                                    ->withJson([
                                        "response" => [
                                            "status" => 200,
                                            "message" => "OK",
                                            "data" => [ "account_type" => "trainee", "username" => $user['trainee_id']
                                        ]
                                    ]], 200);
                    }

                }
            } return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson([
                            "response" => [
                                "status" => 403,
                                "message" => "Access Denied",
                                "error" => "No found user/username"
                            ]
                            ], 403);
        }
    }
    public static function company() {
        if ($db = DB::connect()) {
            $stmt = $db->query("SELECT
            tbl_company.company,
            tbl_trainee.company_id
            FROM tbl_trainee
            LEFT JOIN tbl_company
            ON tbl_trainee.company_id = tbl_company.id
            ORDER BY tbl_company.company");

            while($rows = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                $data = $rows;
            } return $data;
        }
    }

    public static function coaches() {

        if($db = DB::connect()) {

            $stmt = $db->query("SELECT
            tbl_coach.first_name,
            tbl_coach.last_name,
            tbl_trainee.coach_id_incharge
            FROM tbl_trainee
            LEFT JOIN tbl_coach
            ON tbl_trainee.coach_id_incharge = tbl_coach.id
            ORDER BY tbl_trainee.last_name");

            while($rows = $stmt->fetchAll(DB::FETCH_ASSOC)){
                $data = $rows;
            }
            return $data;
        }
    }
    public static function joiner($table = STRING, $tableToGet = STRING ) {
        if ($db = DB::connect()) {
            $stmt = $db->query("SELECT
            tbl_coach.first_name,
            tbl_coach.last_name,
            tbl_coach_schedule.coach_id
            FROM tbl_coach_schedule
            LEFT JOIN tbl_coach
            ON tbl_coach_schedule.coach_id = tbl_coach.id
            ORDER BY tbl_coach.first_name");
            while($row = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                $data = $row;
            }

            return $data;

        }
    }

    public static function curriculumSetDetails($table = STRING, $tableToGet = STRING ) {
        if ($db = DB::connect()) {
            $stmt = $db->query("SELECT
            tbl_curriculum_set_header.curriculum_name,

            FROM tbl_curriculum_set_detail
            LEFT JOIN tbl_curriculum_set_header
            ON tbl_curriculum_set_detail.curriculum_id = tbl_curriculum_set_header.curriculum_id");
            while($row = $stmt->fetchAll(DB::FETCH_ASSOC)) {
                $data = $row;
            }

            return $data;

        }
    }

    public static function deleteRows($tableName = STRING, $key = STRING, $index = STRING, $response) {
        if ($db = DB::connect()) {
            try {
                $db->beginTransaction();
                if ($tableName === 'tbl_trainee') {
                    $table = array(
                        1 => 'tbl_trainee',
                        3 => 'tbl_trainee_schedule_detail',
                        2 => 'tbl_trainee_schedule_header');
                    foreach($table as $i => $k) {
                        $id = DB::cell('tbl_trainee_schedule_header', 'trainee_id', $key)[0];
                        $ids = $id['id'];
                        if ($k === "tbl_trainee_schedule_detail") {
                            $stmt = $db->prepare("DELETE FROM $k WHERE trainee_schedule_id = ?");
                            $stmt->execute([$ids]);
                            continue;
                        }
                        if ($k === "tbl_trainee_schedule_header") {

                            $stmt = $db->prepare("DELETE FROM $k WHERE trainee_id = ?");
                            $stmt->execute([$key]);
                            continue;
                        }
                        $stmt = $db->prepare("DELETE FROM $k WHERE $index = ? ");
                        $stmt->execute([$key]);
                    } $db->commit();
                    return $response
                            ->withHeader("Content-Type", "application/json")
                            ->withJson(array
                            (
                                "response" => array
                                (
                                    "status" => 200,
                                    "message" => "OK",
                                    "data" => array
                                    (
                                        "id" => $index,
                                        "key" => $key,
                                        "details" => "Record successfully deleted"
                                    )
                                )
                        ), 200);
                }
                if ($tableName === 'tbl_trainee_schedule_header') {

                    $table = array(
                        1 => 'tbl_trainee_schedule_header',
                        2 => 'tbl_trainee_schedule_detail',);
                    foreach($table as $i => $k) {

                        if ($k === "tbl_trainee_schedule_detail") {
                            $stmt = $db->prepare("DELETE FROM $k WHERE trainee_schedule_id = ?");
                            $stmt->execute([$key]);
                            continue;
                        }
                        $stmt = $db->prepare("DELETE FROM $k WHERE $index = ? ");
                        $stmt->execute([$key]);
                    }
                    $db->commit();
                        return $response
                                        ->withHeader("Content-Type", "application/json")
                                        ->withJson(array
                                        (
                                            "response" => array
                                            (
                                                "status" => 200,
                                                "message" => "OK",
                                                "data" => array
                                                (
                                                    "id" => $index,
                                                    "key" => $key,
                                                    "details" => "Record successfully deleted"
                                                )
                                            )
                                        ), 200);

                }

                if ($tableName === 'tbl_curriculum_set_header') {
                    $table = array(
                        1 => 'tbl_curriculum_set_header',
                        2 => 'tbl_curriculum_set_detail',
                        3 => 'tbl_curriculum_set_default_time_lessons');

                    foreach($table as $i => $k) {
                        $stmt = $db->prepare("DELETE FROM $k WHERE $index = ? ");
                        $stmt->execute([$key]);
                    }
                    $db->commit();
                        return $response
                                        ->withHeader("Content-Type", "application/json")
                                        ->withJson(array
                                        (
                                            "response" => array
                                            (
                                                "status" => 200,
                                                "message" => "OK",
                                                "data" => array
                                                (
                                                    "id" => $index,
                                                    "key" => $key,
                                                    "details" => "Record successfully deleted"
                                                )
                                            )
                                        ), 200);

                }
                if ($num = $db->query("SELECT COUNT(*) FROM `$tableName` WHERE `$index` = '$key'")) {
                    if ($num->fetchColumn() > 0) {
                        $stmt = $db
                                ->prepare("DELETE FROM `$tableName` WHERE `$index` = ?")
                                ->execute([$key]);
                $db->commit();
                                return $response
                                        ->withHeader("Content-Type", "application/json")
                                        ->withJson(array
                                        (
                                            "response" => array
                                            (
                                                "status" => 200,
                                                "message" => "OK",
                                                "data" => array
                                                (
                                                    "id" => $index,
                                                    "key" => $key,
                                                    "details" => "Record successfully delete with ( $key = $index )"
                                                )
                                            )
                                        ), 200);


                    } else {
                        return $response
                                        ->withHeader("Content-Type", "application/json")
                                        ->withJson(array
                                        (
                                            "response" => array
                                            (
                                                "status" => 400,
                                                "message" => "Bad Request",
                                                "error" => "could not fetch column with ( $key = $index )"
                                            )
                                        ), 400);
                    }
                }
            } catch (DBException $e) {
                $db->rollBack();
                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(array
                (
                    "response" => array
                    (
                        "status" => 400,
                        "message" => "Bad Request",
                        "error" => "Failed to execute query"
                    )
                ), 400);
            }

        }
    }
    /* insert new record */
    public static function insert($tableName = STRING, $data = array()) {
        if ($db = DB::describe($tableName)) {

            /* count the number of columns */
            $columnCount = $db
            ->query("SELECT * FROM $tableName")
            ->columnCount();

            /* count the number of data */
            $dataCount = count($data);
            if ( $dataCount !== $columnCount ) {
                try {
                    throw new Exception("the number of data is lower than the # columns in table[$tableName]");
                } catch(Exception $e) {

                    return $e->getMessage();
                }
            }
        }
    }
    public static function updateData($table = NULL, $data = array()) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $table = $table ? $table : 'tbl_trainee_schedule_detail';
                foreach($data AS $key => $value) {
                    foreach($data[$key] AS $keys => $val ) {
                        if ($keys === 'params') {
                            foreach($data[$key][$keys] AS $k => $j )
                            $object = $k;
                            $index = $j;
                            continue;
                        }
                        foreach($data[$key]['update'] AS $x => $y ) {

                            $stmt = $db->prepare("
                            UPDATE $table SET $x = ? WHERE $object = ?
                            ");
                            $stmt->execute([
                                $y,
                                $index
                            ]);
                        }
                    }
                }

            $db->commit();
            if (isset($y)) {
                return true;
            }

        } catch(PDOException $e) {

            $db->rollBack();
            return false;
        }

    }
    static public function counter(INT $month = NULL, STRING $year = NULL) {
        $db = DB::connect();
        $db->beginTransaction();
        try{
            $month = $month ? $month : date('n', TIME());
            $sYear = date('Y-m-d', mktime( 0,0,0, $month, 1, $year));
            $year = $year ? $sYear : date('Y-m-d', TIME());
            $coach = array();
            $stmt = $db->prepare("SELECT DISTINCT TCS.schedule_date, IFNULL(CONCAT(TC.last_name, ' ', TC.first_name), '') AS names, TC.id AS id
            FROM
            tbl_coach_schedule TCS
            CROSS JOIN tbl_coach TC
            ON TC.id = TCS.coach_id


            WHERE MONTH(TCS.schedule_date) = ? AND YEAR(TCS.schedule_date) = YEAR(?)
            GROUP BY TCS.coach_id
            ORDER BY TCS.coach_id");
            $stmt->execute([$month, $year]);
            while($rows = $stmt->fetchAll()) {
                $names = $rows;
            } $stmt = NULL;


                foreach($names AS $K => $V) {
                    $ID = $names[$K]['id'];
                    $STMT = $db->prepare("SELECT IF( absent_flg, TRUE,FALSE) AS absent_flg, TCS.schedule_date AS class_date FROM tbl_coach_schedule TCS
                    CROSS JOIN tbl_coach TC
                    ON TCS.coach_id = TC.id

                    WHERE TCS.coach_id =  ? AND MONTH(TCS.schedule_date) = ?
                    GROUP BY schedule_date
                    ORDER BY coach_id, schedule_date");
                    $STMT->execute([$ID,$month]);
                    while($rows = $STMT->fetchAll()) {
                        $date = $rows;
                    } $STMT = NULL;

                    $coach['response'][$K]['coach'][0]["id"] =  $V['id'];
                    $coach['response'][$K]['coach'][0]["name"] = $V['names'];

                    foreach ( $date AS $A => $B ) {
                        $sDATE = $date[$A]['class_date'];
                        $STMT = $db->prepare("SELECT DISTINCT coach_id, class_date, lesson_id, COUNT(class_date) AS class_session, class_start_time AS start_time, class_end_time AS end_time FROM tbl_trainee_schedule_detail TCS


                        WHERE coach_id = ? AND coach_id != 0 AND class_date = DATE(?)  AND class_date <> '' AND class_date IS NOT NULL
                        GROUP BY coach_id
                        ORDER BY class_date");
                        $STMT->execute([$ID, $sDATE]);
                        while($rows = $STMT->fetchAll()) {
                            $session = $rows;
                        } $STMT = NULL;

                        $coach['response'][$K]['coach'][0]["schedule"][0]["date"][] = $date[$A]['class_date'];
                        if(isset($session) ){
                        foreach($session AS $J => $L) {
                            if ( $session[$J]['class_date'] === $date[$A]['class_date'] && $session[$J]['coach_id'] === $ID ) {
                            $coach['response'][$K]['coach'][0]["session"][0]['details'][] = array(
                                 
                                    "session_date" => $session[$J]['class_date'],
                                    "session_start_time" => $session[$J]['start_time'],
                                    "session_end_time" => $session[$J]['end_time'],
                                    "class_session" => $session[$J]['class_session'],
                                    "absent_flg" => $names[$K]['absent_flg'],
                                    
                                

                            );
                            continue;
                        }
                        }
                    }
                }
            }









            $db->commit();
            return $coach;
        } catch(PDOException $e) {

            $db->rollBack();
        }

    }
    public static function getClassCounter(INT $month = NULL, STRING $year = NULL) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            $months = array(
                1 => "January",
                2 => "February",
                3 => "March",
                4 => "April",
                5 => "May",
                6 => "June",
                7 => "July",
                8 => "August",
                9 => "September",
                10 => "October",
                11 => "November",
                12 => "December"
            );

            $month = $month ? $month : date('n', TIME());
            $sYear = date('Y-m-d', mktime( 0,0,0, $month, 1, $year));
            $year = $year ? $sYear : date('Y-m-d', TIME());
            $stmt = $db->query("SELECT DISTINCT
            TCS.coach_id AS id, TC.name
            FROM ( SELECT  CONCAT(last_name, ' ', first_name) AS name, id FROM tbl_coach   ) TC
            CROSS JOIN ( SELECT coach_id  FROM tbl_coach_schedule ) TCS
            ON TCS.coach_id = TC.id
            ORDER BY TCS.coach_id

            "); while($rows= $stmt->fetchAll()) { $names = $rows; } $stmt = NULL;
            if (isset($names)) {
                FOREACH ( $names AS $K => $V) {
                    $id = $V['id'];
                    $name = $V['name'];
                    $COLLECTION["response"][$K]['coach'][0]["id"] = $id;
                    $COLLECTION["response"][$K]['coach'][0]["name"] = $name;
                    $stmt = $db->prepare("SELECT schedule_date FROM tbl_coach_schedule WHERE coach_id = ? AND coach_id != 0 AND MONTH(schedule_date) = ? AND YEAR(schedule_date) = YEAR( ? )

                    ORDER BY schedule_date");

                    $stmt->execute([$id, $month, $year]);
                    while($rows= $stmt->fetchAll()) { $dates = $rows; } $stmt = NULL;
                    if (isset($dates)) {
                        FOREACH ($dates AS $J => $L) {
                            $date = $L['schedule_date'];
                            $COLLECTION["response"][$K]['coach'][0]["schedules"][] = $date;


                        $stmt = $db->prepare("SELECT DISTINCT TL.lesson_name AS lesson, TSD.id, TSD.class_date, TSD.coach_id, TSD.class_start_time AS stime, TSD.class_end_time AS etime, COUNT(TSD.class_date) AS sessions, TSD.lesson_id FROM tbl_trainee_schedule_detail TSD

                        CROSS JOIN tbl_lessons TL
                        ON TSD.lesson_id = TL.id

                        WHERE TSD.coach_id = ? AND TSD.coach_id != 0 AND TSD.class_date = DATE( ? ) AND TSD.class_date <> '' AND TSD.class_date IS NOT NULL
                        GROUP BY TSD.class_date
                        ORDER BY TSD.class_date");
                        $stmt->execute([$id, $date]);
                        while($rows= $stmt->fetchAll()) { $session = $rows; } $stmt = NULL;
                            if(isset($session )) {
                                FOREACH ( $session AS $M => $N) {
                                    if( $session[$M]['coach_id'] === $id && $session[$M]['class_date'] === $date) {

                                        $COLLECTION["response"][$K]['coach'][0]["sessions"][] = array(
                                            $N['id'] => array(array(

                                                "session_date" => $N['class_date'],
                                                "session_start" => $N['stime'],
                                                "session_end" => $N['etime'],
                                                "class_session" => $N['sessions'],
                                                "session_lesson_id" => $N['lesson_id'],
                                                "session_lesson" => $N['lesson'],
                                            ))

                                        );
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(! isset($COLLECTION) ) { throw new Exception("shedule not found for the month of $months[$month]."); }
            return $COLLECTION;





            $db->commit();
        } catch(PDOException $e) {
            $db->rollBack();
        }


    }
    public static function schedule($data = array()) {

        list($M1, $D1, $Y1) = explode("-", $data[0]);
        list($M2, $D2, $Y2) = explode("-", $data[1]);

        $date1 = date('Y-m-d H:i:s', mktime(0,0,0,$M1,$D1,$Y1));
        $date2 = date('Y-m-d H:i:s', mktime(0,0,0,$M2,$D2,$Y2));


        $db = DB::connect();
        $db->beginTransaction();
        try {

            $startDate = $date1 < $date2 ? $date1 : $date2;
            $endDate = $date1 > $date2 ? $date1 : $date2;

            $stmt = $db->prepare("
            SELECT TSH.trainee_id, TSD.*
            FROM tbl_trainee_schedule_detail TSD
            LEFT JOIN tbl_trainee_schedule_header TSH
            ON TSD.trainee_schedule_id = TSH.id
            LEFT JOIN
            (
                SELECT MIN(TSDID.id) as id, " . '""' . "AS tempo FROM tbl_trainee_schedule_detail TSDID
                LEFT JOIN tbl_trainee_schedule_header TSHID
                ON TSDID.trainee_schedule_id = TSHID.id
                GROUP BY TSHID.trainee_id, TSDID.lesson_id
            ) AS TSDID
            ON TSDID.id = TSD.id

            WHERE TSD.lesson_id != 0
            AND TSDID.tempo IS NOT NULL
            AND TSD.class_date BETWEEN ? AND ?


            GROUP BY
            TSH.trainee_id, TSD.lesson_id

            ORDER BY
            TSD.class_date ASC, class_start_time ASC");
            $stmt->execute([$startDate, $endDate]);
            while($rows = $stmt->fetchAll()) {
                $schedule = array("first_lesson_occurence" => $rows);
            }
            $stmt = $db->prepare("
            SELECT TSD.*,
            TSH.trainee_id AS trainee_id

            FROM tbl_trainee_schedule_detail TSD
            LEFT JOIN tbl_trainee_schedule_header TSH
            ON TSD.trainee_schedule_id = TSH.id

            WHERE TSD.coach_id = 0
            AND TSD.class_date BETWEEN ? AND ?
            AND TSD.lesson_id != 0

            ORDER BY
            TSD.class_date ASC, class_start_time ASC");
            $stmt->execute([$startDate, $endDate]);
            while ($rows = $stmt->fetchAll()) {
                $schedule["unassigned"] = $rows;
            }

            $stmt = $db->prepare("
            SELECT * FROM tbl_trainee_schedule_detail
            WHERE coach_id != 0
            AND class_date BETWEEN ? AND ?
            AND lesson_id != 0
            ");
            $stmt->execute([$startDate, $endDate]);
            while ($rows = $stmt->fetchAll()) {
                $schedule["assigned"] = $rows;
            }
            $stmt = $db->prepare("
            SELECT *,
            TC.type
            FROM tbl_coach_schedule TCS

            LEFT JOIN tbl_coach TC
            ON TCS.coach_id = TC.id

            WHERE schedule_date BETWEEN ? AND ?
            AND absent_flg = 0
            ");
            $stmt->execute([$startDate, $endDate]);
            while ($rows = $stmt->fetchAll()) {
                $schedule["coach_schedule"] = $rows;
            }

            $stmt = $db->prepare("
            SELECT *
            FROM tbl_trainee

            WHERE study_period_start_date BETWEEN ? AND ?
            ");
            $stmt->execute([$startDate, $endDate]);
            while ($rows = $stmt->fetchAll()) {
                $schedule["trainee"] = $rows;
            }
            if (isset($schedule)) {
                return $schedule;
            } else {
                return false;
            }

            $db->commit();
            return $schedule;
        } catch(PDOException $e ) {

            $db->rollBack();
            return false;
        }



    }
    public static function checkDate($date, $param, $method, $id ) {
        $db = DB::connect();
        $db->beginTransaction();
        try {
            if ( $method === "PUT" ) {
                $stmt = $db->prepare("
                SELECT IF ( schedule_date = DATE( ? ), FALSE, TRUE ) AS schedule_date FROM tbl_coach_schedule TC WHERE id = ?");
                $stmt->execute([$date, $id]);
                while ($rows = $stmt->fetchAll()) {
                    $data = $rows;
                } if ( $data[0]['schedule_date'] ){
                    $stmt = $db->prepare("SELECT schedule_date FROM tbl_coach_schedule WHERE schedule_date = DATE('$date') AND coach_id = ?");
                    $stmt->execute([$param]);

                    while($row = $stmt->fetchAll()) {
                        $data = $row;
                    }
                    if(isset($data)) {

                        foreach( $data AS $keys => $value ) {
                             foreach($value AS $k => $j) {
                                 if ( $date === $j ) {
                                     return true;
                                 }
                             }
                        }
                    } else {
                        return false;
                    }
                } else {

                    return false;
                }
            $db->commit();
            } else {
                $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE schedule_date = DATE( ? ) && coach_id = ?");
                $stmt->execute([$date, (integer)$param]);
                while($row = $stmt->fetchAll()) {
                    $data = $row;
                }
                if(isset($data)) {
                    return true;
                } else {
                    return false;
                }
            $db->commit();
            }
        } catch(DBException $e) {

            $db->rollBack();
        }

    }


  public static function checkCoachId( $id, $data, $p, $i) {
    $db = DB::connect();
    $db->beginTransaction();
    $coachId = $data['coach_id'];
    try {
    $details = $db->prepare("SELECT * FROM tbl_trainee_schedule_detail WHERE id = ?");
    $details->execute([$i]);
    while ($A = $details->fetchAll()) {
         $detail = $A;
    }
    if (! isset($detail)) return 4;
    if ($coachId === "0" || 0 ) {
        foreach($data AS $k => $v) {
            $stmt = $db->prepare("UPDATE tbl_trainee_schedule_detail SET ? = ? WHERE id = '$i'");
            $stmt->execute([$k,$v]);
            $stmt = NULL;
        }
        $db->commit();
        return 6;
    }
    $classDate = isset($data['class_date']) ? date('Y-m-d H:i:s', strtotime($data['class_date'])) : date('Y-m-d H:i:s', strtotime($detail[0]['class_date']));
    $startTime = isset($data['class_start_time']) ? date('Y-m-d H:i:s', strtotime($data['class_start_time'])) : date('Y-m-d H:i:s', strtotime($detail[0]['class_start_time']));
    $endTime = isset($data['class_start_time']) ? date('Y-m-d H:i:s', strtotime($data['class_end_time'])) : date('Y-m-d H:i:s', strtotime($detail[0]['class_end_time']));
      if (count( $data ) <= 1) {
        $stmt = $db->prepare("SELECT schedule_date FROM tbl_coach_schedule WHERE coach_id = ? ORDER BY schedule_date");
        $stmt->execute([$coachId]);
            while($A = $stmt->fetchAll()){
                foreach($A as $k => $v) {
                    $B[] = $v;
                }
            }
            if (isset($B)) {
                $start = $B[0]['schedule_date'];
                $end = $B[count($B) - 1]['schedule_date'];

             $stmt = NULL;

            $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE coach_id = ? AND DATE('$classDate') BETWEEN DATE(?) AND DATE(?) ORDER BY schedule_date");
            $stmt->execute([(integer)$coachId, $start, $end]);
            while ($rows = $stmt->fetchAll()) {
                $C = $rows;
            }
            if(isset($C)) {
               $stmt = NULL;
               $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE schedule_date = DATE('$classDate') AND TIME(?) BETWEEN schedule_start_time AND schedule_end_time");
               $stmt->execute([$startTime]);
               while($rows = $stmt->fetchAll()) {
                   $D = $rows;
               }
               if (isset($D)) {
                   $stmt = NULL;
                   $stmt = $db->prepare("UPDATE tbl_trainee_schedule_detail SET coach_id = ? WHERE id = ?");
                   $stmt->execute([$coachId, $i]);
                   $db->commit();
                   return 3;
               } else {

                    return 2;
               }

            } else {
                return 1;
            }
        } else {

            return 0;
        }

      }

      $k = $p;


      $stmt = $db->prepare("SELECT coach_id FROM tbl_coach_schedule WHERE coach_id = ?
      ");
      $stmt->execute([$coachId]);
      if ( $stmt->rowCount() > 0 ) {
        $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE coach_id = ? AND schedule_date = ?");
        $stmt->execute([
          $coachId,
          $classDate
        ]);

        if ( $stmt->rowCount() > 0 ) {
            $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE TIME(?) BETWEEN schedule_start_time AND schedule_end_time");
            $stmt->execute([$startTime]);
            if ( $stmt->rowCount() > 0 ) {
              foreach ($data AS $key => $value) {
                switch ( $key ) {
                  case 'class_date': $v = date('Y-m-d H:i:s', strtotime($value));
                  break;
                  case 'class_start_time': $v = date('Y-m-d H:i:s', strtotime($value));
                  break;
                  case 'class_end_time' : $v = date('Y-m-d H:i:s', strtotime($value));
                  break;
                  default:
                  $v = $value;
                    break;
                }
                $stmt = $db->prepare("UPDATE tbl_trainee_schedule_detail SET $key = ? WHERE $p = ?");
                $stmt->execute([$v, $i]);
                $stmt = null;

              }

              $db->commit();
              return 3;

            } else {
              return 2;
            }

        } else {

          return 1;
        }
      } else {
        return 0;
      }
    } catch (PDOException $e) {
      $db->rollBack();
      return false;
    }
  }



  public static function manageCoaches( $D, $P ) {
    $db = DB::connect();
    $db->beginTransaction();
        try {
            $DEFAULT = $sDATE = $eDATE = NULL;
            $INSERT = "";
            // TABLES
            $TSC = "tbl_trainee_schedule_detail";
            $TSH = "tbl_trainee_schedule_header";
            $COUNTER = 0;
            // DEFAULT VALUES
            $STMT = $db->prepare("SELECT * FROM $TSC WHERE id = ?");
            $STMT->execute([$P]);
            while($rows = $STMT->fetchAll()){
                $DEFAULT = $rows;
            } $STMT = NULL;

            $STMT = $db->prepare("SELECT schedule_date FROM tbl_coach_schedule WHERE id = ? ORDER BY schedule_date");
            $STMT->execute([$DEFAULT[0]['coach_id']]);
            while($rows = $STMT->fetchAll()){
                $sDATE = $rows[0]['schedule_date'];
                $eDATE = $rows[count($rows) - 1]['schedule_date'];
            } $STMT = NULL;

            // PARAMETERS
            $ID = ( isset( $D['coach_id'] ) ) ? (integer)$D['coach_id'] : (integer)$DEFAULT[0]['coach_id'];
            $remarks = ( isset( $D['remarks'] ) ) ? $D['remarks'] : $DEFAULT[0]['remarks'];
            $TRAINEE = ( isset( $D['trainee_schedule_id'] ) && !empty( $D['trainee_schedule_id'] ) ) ? (integer)$D['trainee_schedule_id'] : (integer)$DEFAULT[0]['trainee_schedule_id'];
            $LESSON = ( isset( $D['lesson_id'] ) && !empty( $D['lesson_id'] ) ) ? (integer)$D['lesson_id'] : (integer)$DEFAULT[0]['lesson_id'];
            $DATE = ( isset( $D['class_date'] ) && !empty( $D['class_date'] )) ? date('Y-m-d', strtotime($D['class_date'])) : date('Y-m-d', strtotime($DEFAULT[0]['class_date']));
            $sTIME = ( isset( $D['class_start_time'] ) && !empty( $D['class_start_time'] )) ? date('H:i:s', strtotime($D['class_start_time'])) : date('H:i:s', strtotime($DEFAULT[0]['class_start_time']));
            $eTIME = ( isset( $D['class_end_time'] ) && !empty( $D['class_end_time'] )) ? date('H:i:s', strtotime($D['class_end_time'])) : date('H:i:s', strtotime($DEFAULT[0]['class_end_time']));
            $FIELDS = array("coach_id" => $ID, "lesson_id" => $LESSON, "class_date" =>  $DATE, "class_start_time" => $sTIME, "class_end_time" => $eTIME, "trainee_schedule_id" => $TRAINEE, "remarks" => $remarks );
            $LEN = count($FIELDS);
            //DEBUGGER

            if ( $ID ) {
                foreach($FIELDS AS $K => $V) {
                    $STMT = $db->prepare("SELECT IF( coach_id <> ? OR class_date <> DATE(?) OR class_start_time <> TIME(?) , TRUE, FALSE ) AS t FROM $TSC TSC WHERE id = ?");
                    $STMT->execute([$ID, $DATE, $sTIME, $P]);

                    while($rows = $STMT->fetchAll()) {
                        $COACH_SCHEDULE_EXISTS = $rows[0]['t'];
                    } $STMT = NULL;
                    if (isset($COACH_SCHEDULE_EXISTS) && $COACH_SCHEDULE_EXISTS ) {
                        $STMT = $db->prepare("SELECT COUNT( class_date ) AS class_date FROM $TSC WHERE coach_id = ? AND class_date = DATE(?) AND class_start_time = TIME( ? ) GROUP BY class_date");
                        $STMT->execute([$ID, $DATE, $sTIME]);
                        while($rows = $STMT->fetchAll()) {
                            $SCHEDULE_EXISTS = $rows[0]['class_date'];
                        } $STMT = NULL;
                        if (isset($SCHEDULE_EXISTS)) {
                            echo "DETECTED\n\n";
                            echo "schedule exists!";
                            return 8;
                        }

                    } $STMT = NULL;
                    $COUNTER++;
                    if( $K === 'coach_id') { //VALIDATE COACH

                        // CHECK IF THE COACH EXISTS IN THE COACH TABLE
                        $STMT = $db->prepare("
                        SELECT TC.schedule_date AS dates
                        FROM tbl_coach_schedule TC
                        WHERE coach_id = ? ORDER BY dates");
                        $STMT->execute([$V]);

                        // COLLECT ALL AVILABLE CLASS DATE ON SELECTED COACH
                        while($rows = $STMT->fetchAll()) {
                            $DATA = $rows;
                        } $STMT = NULL;

                        if(! isset($DATA)) return 0; // RETURN FALSE IF NO AVAILABLE COACH FOR THE CHOSEN COACH #ID
                        $sDATE = $DATA[0]['dates']; //START DATE
                        $eDATE = $DATA[count($DATA) - 1]['dates'];  //END DATE

                    }
                    if ($K === 'class_date') { // VALIDATE COACH FROM THE REGISTERED COACHES
                        $STMT = $db->prepare("SELECT IF(TSC.class_date <> ?, ( ? ), FALSE) AS dates FROM $TSC TSC WHERE id = ?");
                        $STMT->execute([$V, $V, $P]);
                        while ($rows = $STMT->fetchAll()) {
                            $cDATE = $rows[0]['dates'];
                        } $STMT = NULL;
                        if( isset( $cDATE ) && $cDATE ) {
                            $STMT = $db->prepare("SELECT schedule_date FROM tbl_coach_schedule WHERE coach_id = ? AND schedule_date = DATE( ? ) AND DATE( ? ) BETWEEN DATE( ? ) AND DATE( ? )");
                            $STMT->execute([$ID, $DATE, $DATE, $sDATE, $eDATE]);

                            while($rows = $STMT->fetchAll()) {
                                $DATE_RANGE = $rows[0]['schedule_date'];
                            } $STMT = NULL;
                            if (! isset($DATE_RANGE)) return 1; // COACH DATE IS OUT OF RANGE
                        }
                    }
                    if ($K === 'class_start_time') { // VALIDATE TIME SCHEDULE
                        $STMT = $db->prepare("SELECT IF( TCS.class_start_time <> TIME( ? ) , TRUE, FALSE ) AS t FROM $TSC TCS WHERE id = ?");
                        $STMT->execute([$sTIME, $P]);
                        while ($rows = $STMT->fetchAll()) {
                            $TIME_RANGE = $rows[0]['t'];
                        } $STMT = NULL;

                        if (isset( $TIME_RANGE ) && $TIME_RANGE ) {
                           // SCHEDULE TIME HAS BEEN CHANGE
                            $STMT = $db->prepare("SELECT schedule_start_time FROM tbl_coach_schedule WHERE coach_id = ? AND schedule_date = DATE( ? ) AND TIME( ? ) BETWEEN TIME( schedule_start_time ) AND TIME( schedule_end_time )");
                            $STMT->execute([$ID, $DATE, $sTIME]);
                            while ( $rows = $STMT->fetchAll()) {
                                $WITHIN_TIME_RANGE = $rows[0]["schedule_start_time"];
                            } $STMT = NULL;
                            if (! isset($WITHIN_TIME_RANGE)) return 2; // CHECK IF THE TIME IS IN RANGE
                        }
                    }

                    if ( $K === 'trainee_schedule_id') { // VALIDATE TRAINEE
                        $STMT = $db->prepare("SELECT IF( TCS.trainee_schedule_id <> ?, TRUE, FALSE) AS t FROM $TSC TCS WHERE id = ?");
                        $STMT->execute([$TRAINEE, $P]);
                        while ($rows = $STMT->fetchAll()) {
                            $TRAINEE_ALTERED = $rows[0]['t'];
                        } $STMT = NULL;
                        if ( isset($TRAINEE_ALTERED) && $TRAINEE_ALTERED ) {

                            $STMT = $db->prepare("SELECT * FROM $TSH WHERE id = ?");
                            $STMT->execute([$TRAINEE]);
                            while($rows = $STMT->fetchAll()) {
                                $TRAINEE_ID = $rows[0]['trainee_id'];
                            } $STMT = NULL;
                            if (isset($TRAINEE_ID)) { // CHECK IF TRAINEE EXISTS

                                $STMT = $db->prepare("SELECT * FROM tbl_trainee WHERE trainee_id = ?");
                                $STMT->execute([$TRAINEE_ID]);
                                while($rows = $STMT->fetchAll()) {
                                    $TRAINEES = $rows;
                                } $STMT = NULL;

                                // GATHER ALL DATA FOR QUERYING TRAINEE
                                if (! isset($TRAINEES)) return 3;
                                $TRAINEE_START_PERIOD = date('Y-m-d', strtotime($TRAINEES[0]['study_period_start_date']));
                                $TRAINEE_END_PERIOD = date('Y-m-d', strtotime($TRAINEES[0]['study_period_end_date']));


                                $STMT = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE coach_id = ? AND schedule_date = DATE( ? ) AND DATE( ? ) BETWEEN DATE( ? ) AND DATE( ? )");
                                $STMT->execute([$ID, $TRAINEE_START_PERIOD, $TRAINEE_START_PERIOD, $sDATE, $eDATE]);

                                while($rows = $STMT->fetchAll()) {
                                    $TRAINEE_DATE_IN_RANGE = $rows;
                                } $STMT = NULL;

                                if(! isset($TRAINEE_DATE_IN_RANGE)) return 4;

                                $STMT = $db->prepare("SELECT * FROM $TSH WHERE trainee_id = ?");
                                $STMT->execute([$TRAINEES[0]['trainee_id']]);
                                if ($rows = $STMT->fetchAll()) {
                                    $HEADER = $rows[0]['id'];
                                } $STMT = NULL;

                                if(! isset($HEADER)) return 5;

                                $STMT = $db->prepare("SELECT * FROM $TSC WHERE trainee_schedule_id = ? AND class_date = DATE( ? ) AND class_start_time = TIME( ? ) AND coach_id = ?");
                                $STMT->execute([$HEADER, $DATE, $sTIME, $ID ]);
                                $ROWCOUNT = $STMT->rowCount();
                                    if ($ROWCOUNT > 0 ) return 6;
                                $STMT = NULL;




                            } else { return 5; } // NO TRAINEE SCHEDULE FOUND
                        }
                    }

                    // PREPARING FIELDS FOR UPDATE
                    if ( $V ) {
                        if ($COUNTER === $LEN ) {
                            $INSERT .= "$K = '$V'";
                            continue;
                        } $INSERT .= "$K = '$V', ";
                    }
                }

                $db->exec("UPDATE $TSC SET $INSERT WHERE id = '$P'");
                $db->commit();
                return 7;
            }
            else {
                foreach($FIELDS AS $K => $V ) {
                $COUNTER++;
                    if ($COUNTER === $LEN ) {
                        $INSERT .= "$K = '$V'";
                        continue;
                    } $INSERT .= "$K = '$V', ";

                }
                $db->exec("UPDATE $TSC SET $INSERT WHERE id = '$P'");
                $db->commit();
                return 7;
            }


        } catch(Exception $e) {
            $db->rollBack();
            return 10;
        }




    return 0;


  }
  public static function updateCoach($d, $param, $index ) {
    $db = DB::connect();

    try {
      $db->beginTransaction();
      foreach ($d as $key => $value) {
        echo $key;

      }
      $db->commit();
      return true;
    } catch (PDOException $e) {
      $db->rollBack();
      return false;
    }


  }

  static public function unassigned($P, $V, $response) {
    $coachId = 0;
    $db = DB::connect();
    $db->beginTransaction();
    try {

        $stmt = $db->prepare("SELECT * FROM tbl_trainee_schedule_detail WHERE id = ?");
        $stmt->execute([$P]);
        while($rows = $stmt->fetchAll()) {
            $detail = $rows;
        }
        if (isset($detail)) {

            $stmt = NULL;
            $traineeId = $detail[0]['trainee_schedule_id'];
            $classDate = $detail[0]['class_date'];
            $classStartTime = $startTime = $detail[0]['class_start_time'];
            $classEndTime = $endDate = $detail[0]['class_end_time'];
            $stmt = $db->prepare("SELECT trainee_id FROM tbl_trainee_schedule_header WHERE id = ?");
            $stmt->execute([$traineeId]);
            while ($rows = $stmt->fetchAll()) {
                $trainee = $rows;
            }
            if (isset($trainee)) {
                $stmt = NULL;
                $TID = $trainee[0]['trainee_id'];
                $stmt = $db->prepare("SELECT * FROM tbl_trainee WHERE trainee_id = ?");
                $stmt->execute([$TID]);
                while($rows = $stmt->fetchAll()) {
                    $trainees = $rows;
                }
                if (! isset($trainees )) return 3;
                $stmt = NULL;
                $startPeriod = date('Y-m-d', strtotime((string)$trainees[0]['study_period_start_date']));
                $endPeriod = date('Y-m-d', strtotime((string)$trainees[0]['study_period_end_date']));

            } else {
                $db->rollBack();
                return 3;
            }

            foreach($V AS $key => $value) {
                if ($key === 'coach_id') {
                    $coachId = $value;
                    continue;
                }
                if ($key === "class_date") {
                    $classDate = date('Y-m-d', strtotime((string)$V['class_date']));
                    continue;
                }
                if ($key === "class_start_time") {
                    $startTime = date('H:i:s', strtotime($V['class_start_time']));
                    continue;
                }
                if ($key === "class_end_time") {
                    $endTime = date('H:i:s', strtotime($V['class_end_time']));
                    continue;
                }
            }
            if ($coachId === '0' || 0 ) {
                $stmt = $db->exec("UPDATE tbl_trainee_schedule_detail SET coach_id = '0' WHERE id = '$P'");
                return 1;
            }
            $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE coach_id = ? ORDER BY schedule_date");
            $stmt->execute([$coachId]);
            while($rows = $stmt->fetchAll()) {
                $coach = $rows;
            }
            if (isset($coach)) {
                $stmt = NULL;
                $startDate = date('Y-m-d', strtotime($coach[0]['schedule_date']));
                $endDate = date('Y-m-d', strtotime($coach[count($coach) - 1]['schedule_date']));

                $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule  WHERE coach_id = ? AND DATE('$classDate') BETWEEN DATE(?) AND DATE(?)");
                $stmt->execute([$coachId, $startDate, $endDate]);
                while($rows = $stmt->fetchAll()) {

                    $coaches = $rows;
                }

                if(isset($coaches)) {
                    $stmt = NULL;

                    $stmt = $db->prepare("SELECT * FROM tbl_trainee WHERE trainee_id = ? AND DATE('$classDate') BETWEEN DATE(?) AND DATE(?)");
                    $stmt->execute([$TID, $startPeriod, $endPeriod]);
                    while($rows = $stmt->fetchAll()) {
                        $trainees = $rows;
                    }

                    if (isset($trainees)) {
                        $stmt = NULL;

                        $stmt = $db->prepare("SELECT * FROM tbl_trainee_schedule_detail WHERE trainee_schedule_id = ? AND class_date = DATE('$classDate') AND class_start_time = TIME('$startTime')");
                        $stmt->execute([$traineeId]);
                        while ( $rows = $stmt->fetchAll()) {
                            $time = $rows;
                        }
                        if (isset($time)) {
                            return 6;
                        }
                        $stmt = NULL;
                        $stmt = $db->prepare("SELECT * FROM tbl_coach_schedule WHERE coach_id = ? AND schedule_date = ? AND TIME(?) BETWEEN schedule_start_time AND schedule_end_time");
                        $stmt->execute([$coachId,$classDate,$startTime]);

                        while($rows = $stmt->fetchAll()) {

                            $coachTime = $rows;
                        }
                        if (isset($coachTime)) {
                            $stmt = NULL;
                            $stmt = $db->prepare("UPDATE tbl_trainee_schedule_detail SET coach_id = ?, class_date = ?, class_start_time = ?, class_end_time = ?  WHERE id = ?");
                            $stmt->execute([$coachId, $classDate, $startTime, $endTime, $P]);
                            $db->commit();
                            return 8;
                        } else {
                            $db->rollBack();
                            return 7;
                        }
                    } else {

                        $db->rollBack();
                        return 5;
                    }

                } else {
                    $db->rollBack();
                    return 4;
                }

            } else  {
                // COULD NOT GET COACH #id
                $db->rollBack();
                return 2;
            }

        } else {
            // COULD NOT GET #id
            $db->rollBack();
            return 0;
        }
    } catch(Handler $e) {
        $db->rollBack();
        echo $e->getMessage();

    }



  }
}
