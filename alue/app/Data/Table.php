<?php

namespace ALUE\Data;
require "spp.class.php";
use \ALUE\INI\INI;
use SSP;

class Table {

   public static function display($response = NULL, $primaryKey = STRING, $tableName = STRING, $data = array()) {
    $columns = array();
    $dbCredentials = array
    (
        "user" => INI::get('username'),
        'pass' => INI::get('password'),
        'db' => INI::get('database'),
        'host' => INI::get('servername')
    );
    foreach ($data as $key => $val) {
       
        
        $columns[] = array
        (
            "db" => $key, "dt" => $val
        );


    }

    return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson(SSP::simple($_GET, $dbCredentials, $tableName, $primaryKey, $columns), 200);

   }

}