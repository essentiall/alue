<?php
date_default_timezone_set('Asia/Kuala_Lumpur');
use \Firebase\JWT\JWT as JWT;
use \ALUE\Token\Access as Access;
use \ALUE\Method\Handler as Handler;
use \ALUE\Data\Table as Table;
use \ALUE\Database\Connection as Connection;
use \ALUE\INI\INI as INI;
use \ALUE\DB\DB;
$app->group('/trainee', function() use($app){

    $app->map(['GET', 'POST', 'DELETE', 'PUT'], '/[{id}]', function($request, $response, $args) {
    require "spp.class.php";
    $database = new Connection;
    $bearer = new Access;
    $token = $bearer->getBearerToken();
    $param = $request->getAttribute('id');
    $method = $request->getMethod();
    $keys = $request->getParsedBody() ? $request->getParsedBody() : NULL;
    $dbCredentials = array
        (
            "user" => INI::get('username'),
            'pass' => INI::get('password'),
            'db' => INI::get('database'),
            'host' => INI::get('servername')
        );
        try {
            $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));

            $data = array();
            if ($method === 'GET' ){
            if ($param === 'get') {
                return DB::getAllRows('tbl_coach_schedule', $response,'tbl_trainee');
                exit;
            }
                return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(ssp::simple($_GET, $dbCredentials, 'tbl_trainee', 'trainee_id',  array
            (

                array("db" => "trainee_id", "dt" => 0),
                array("db" => "trainee_id", "dt" => 1, "formatter" => function($d, $row){
                    $id = $row['trainee_id'];
                    $tId = DB::cell('tbl_trainee_schedule_header', 'trainee_id', $id )[0];
                    $sd = DB::cell('tbl_trainee_schedule_detail', 'trainee_schedule_id', $tId['id'])[0];
                    $schId = $tId['id'];
                    $cName = DB::cell('tbl_curriculum_set_header', 'curriculum_id', $tId['curriculum_id'])[0];
                    $cur = $cName['curriculum_name'];
                    if ($tId['generated_flg']) {
                        $link = "<a href='javascript:void(0)' class='btn-show-trainee-schedule' data-trainee-schedule-id='$schId' >$cur</a>";
                    } else {
                        $link = "<a href='javascript:void(0)'>$cur</a>";
                    }

                    return $link;
                }),
                array("db" => "last_name", "dt" => 2),
                array("db" => "first_name", "dt" => 3),
                array("db" => "company_id", "dt" => 4, "formatter" => function($d, $row){
                    $data = DB::company();
                        foreach($data as $key => $value ) {
                            if( (integer)$data[$key]['company_id'] === (integer)$d ) {

                                $companyName = $data[$key]['company'];
                            }
                        }
                    return $companyName;
                }),
                array("db" => "coach_id_incharge", "dt" => 5, "formatter" => function($d, $row){
                    $data = DB::coaches();
                        foreach($data as $key => $value ) {
                            if( (integer)$data[$key]['coach_id_incharge'] === (integer)$d ) {

                                $name = $data[$key]['first_name'] . " " . $data[$key]['last_name'];
                            }
                        }
                    return $name;
                }),
                array("db" => "num_weeks", "dt" => 6),
                array("db" => "study_period_start_date", "dt" => 7, "formatter" => function($d, $row){
                    return date('n/j/Y', strtotime($d));
                }),
                array("db" => "study_period_end_date", "dt" => 8, "formatter" => function($d, $row){
                    return date('n/j/Y', strtotime($d));
                }),
                array("db" => "initial_assessment_lvl", "dt" => 9),
                array("db" => "type", "dt" => 10),
                array("db" => "trainee_id", "dt" => 11, "formatter" => function($d, $row ) {
                    $id = $row['trainee_id'];
                    $tId = DB::cell('tbl_trainee_schedule_header', 'trainee_id', $id )[0];
                    $schId = $tId['id'];
                    if ($tId['generated_flg']) {
                        $link = '<a class="btn-show-trainee-schedule" data-trainee-schedule-id="' . $schId .'" data-id="' . $d . '" href="javascript:void(0);">View Schedule</a> | <a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a>';
                    } else {
                        $link = '<a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a>';

                    }
                    return $link;
                }),
            )), 200 );

            }
            if ($method === 'DELETE') {
                return Handler::method($method,$request, $response, $data, 'tbl_trainee');
            }
            if (is_array($keys)) {
                foreach($keys as $key => $value) {
                    if ($key === 'trainee_id') {
                        if ($database->row('tbl_trainee', 'trainee_id', $value) && $method == 'POST') {
                            return $response
                            ->withHeader("Content-Type", "application/json")
                            ->withJson(array
                                (
                                    "response" => array
                                    (
                                        "status" => 400,
                                        "message" => "Not Acceptable",
                                        "error" => "duplicated record"
                                    )
                                ), 400
                            );
                        }
                    }
                    if ($key === 'study_period_start_date' || $key === 'study_period_end_date') {
                        $data[$key] = date('Y-m-d H:i:s', strtotime("$value"));
                        continue;
                    }
                    $data[$key] =  $value;
                }
                return Handler::method($method , $request, $response, $data, 'tbl_trainee');
            } else {
                throw new Exception("no data or null key's");
            }
        } catch(Exception $e) {

            if( $e->getMessage() == "no data or null key's") {
                $message = "Bad Request";
                $status = 400;
            } else {
                $message = "Unauthorized";
                $status = 401;
            }
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(
                array(
                    "response" => array(
                        "status" => $status,
                        "message" => $message,
                        "error" => $e->getMessage()
                    )
                ), $status
            );
        }
    });
    $app->group('/schedule', function() use($app){
        //HEADER
        $app->map(['GET', 'POST', 'PUT', 'DELETE'], '/header/[{id}]', function($request, $response){
        require "spp.class.php";
        $database = new Connection;
        $bearer = new Access;
        $token = $bearer->getBearerToken();
        $param = $request->getAttribute('id');
        $method = $request->getMethod();
        $data = $request->getParsedBody() ? $request->getParsedBody() : NULL;
        $dbCredentials = array
            (
                "user" => INI::get('username'),
                'pass' => INI::get('password'),
                'db' => INI::get('database'),
                'host' => INI::get('servername')
            );

            try {
                $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
                if($method === 'GET') {
                    if ($param === 'get') {
                        return DB::getAllRows('tbl_trainee_schedule_header', $response );
                        exit;
                    }

                    return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(ssp::simple($_GET, $dbCredentials, 'tbl_trainee_schedule_header', 'id',
                    array(
                            array("db" => "id", "dt" => 0),
                            array("db" => "curriculum_id", "dt" => 1, "formatter" => function($d, $row ) {

                                return DB::cell('tbl_curriculum_set_header', 'curriculum_id', $d)[0]['curriculum_name'];

                            }),
                            array("db" => "trainee_id", "dt" => 2, "formatter" => function($d, $row ) {

                                $cell = DB::cell('tbl_trainee', 'trainee_id', $d)[0];
                                return $cell['last_name'] . " " . $cell['first_name'];

                        }),
                        array("db" => "trainee_id", "dt" => 3, "formatter" => function($d, $row ) {
                            $cell2 = DB::cell('tbl_trainee', 'trainee_id', $d)[0];
                            $cell = DB::cell('tbl_company', 'id', $cell2['company_id'])[0];

                            return $cell['complete_name'];

                    }),
                            array("db" => "generated_flg", "dt" => 4, "formatter" => function($d, $row ) {

                                if ( $d > 0 ) {
                                    return "true";
                                } else {
                                    return "false";
                                }

                        }),
                            array("db" => "id", "dt" => 5,  "formatter" => function($d, $row ) {

                                if((integer)$row['generated_flg'] > 0) {
                                    return '<a class="btn-show-edit-trainee-schedule-form" data-id="' . $d . '" data-trainee-id="' . $row['trainee_id'] .'" href="javascript:void(0);">Edit</a>';
                                }
                                return '<a class="btn-show-generate-trainee-schedule-form" data-id="' . $d . '" data-trainee-id="' . $row['trainee_id'] .'" href="javascript:void(0);">Generate</a>';
                            }),
                        )
                    ), 200);
                }
                if (! $data && $method == "POST") {
                    throw new Exception('required key\'s are missing');
                } else {

                    return Handler::method($method, $request, $response, $data,'tbl_trainee_schedule_header');
                }
            }
            catch (Exception $e) {
                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(
                    array(
                        "response" => array(
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => $e->getMessage()
                        )
                    ), 401
                );
            }
        });

        $app->map(["GET", "POST", "PUT", "DELETE"], '/details/[{id}]', function($request, $response, $arguments){
            $database = new Connection;
            $bearer = new Access;

            $token = $bearer->getBearerToken();
            $param = $request->getAttribute('id');
            $method = $request->getMethod();
            $data = $request->getParsedBody() ? $request->getParsedBody() : NULL;
            $parameter = isset($_GET['par']) ? $_GET['par'] : "";
            $index = isset($_GET['index']) ? $_GET['index'] : "";
            try {
                $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));

                if ($method === 'GET') { //REQUEST AND PULL DATA
                    if ($param === 'get') {
                        return DB::getTraineeDetails('tbl_trainee_schedule_header', $parameter, $index, $response);
                    }
                }
                if ($method === "POST") {
                    $data = $request->getBody();
                    $raw = json_decode($data->getContents(), true);
                    return DB::addTraineeSchedule('tbl_trainee_schedule_detail', $raw, $response);
                }
                if ($method === "PUT") {

                    /* switch(DB::manageCoaches( $data, $param )) {
                        case 0: 
                            throw new Exception("coach does not exists please select another coach id.", 400);
                        break;

                    } */





                      switch(DB::checkCoachId($param, $data, $parameter, $param)) {
                      case 0 :
                      $status = 400;
                      throw new Exception("coach is not available please select a different coach.");
                      break;
                      case 1:
                      $status = 400;
                      throw new Exception("class date is out of range.");
                      break;
                      case 2:
                      $status = 400;
                      throw new Exception("time is out of range.");
                      break;
                      case 3:
                         return $response->withHeader("Content-Type", "application/json")
                         ->withJson(array(
                           "response" => array(
                             "status" => 200,
                             "message" => "Ok",
                             "details" => "record updated successfully!"
                           )
                         ), 200 );
                      break;
                      case 4:
                      $status = 400;
                      throw new Exception("schedule id not found.");
                      break;
                      case 5:
                      $status = 400;
                      throw new Exception("database error could not fetch fields time: start | end");
                      break;
                      case 6:
                      $status = 200;
                      return $response->withHeader("Content-Type", "application/json")
                         ->withJson(array(
                           "response" => array(
                             "status" => 200,
                             "message" => "Ok",
                             "details" => "coach id has been set to 0 with fieil #ID: $param!"
                           )
                         ), 200 );
                      break;
                      } 

                }

                    return Handler::method($method, $request, $response, $data,'tbl_trainee_schedule_detail');

            } catch(Exception $e) {
                switch ($e->getCode()) {
                    case 400: 
                        $message = "Bad Request";
                    break;
                    case 401:
                        $message = "Unauthorized";
                    break;
                }
                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(
                    array(
                        "response" => array(
                            "status" => $e->getCode(),
                            "message" => $message,
                            "error" => $e->getMessage()
                        )
                    ), $e->getCode()
                );
            }
        });
        $app->map(["GET"], '/details/assigned/[{id}]', function($request, $response, $arguments){
            require "spp.class.php";
            $bearer = new Access;
            $token = $bearer->getBearerToken();
            $method = $request->getMethod();
            $attribute = $request->getAttribute('id');
            $data = $request->getParsedBody() ? $request->getParsedBody() : NULL;
            $dbCredentials = array
            (
                "user" => INI::get('username'),
                'pass' => INI::get('password'),
                'db' => INI::get('database'),
                'host' => INI::get('servername')
            );
            try {
                $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));

                DB::deletTableRows('tmp_assigned_trainee_schedule_detail');

                if(DB::signedCoaches()){
                return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(ssp::simple($_GET, $dbCredentials, 'tmp_assigned_trainee_schedule_detail', 'id', array(
                        array("db" => "id", "dt" => 0),
                        array("db" => "trainee_name", "dt" => 1),
                        array("db" => "class_date", "dt" => 2, "formatter" => function($d, $row){
                            return date('m/j/Y', strtotime($d));
                        }),
                        array("db" => "start_time", "dt" => 3,"formatter" => function($d, $row){
                            return date('G:i', strtotime($d));
                        }),
                        array("db" => "lesson_name", "dt" => 4),
                        array("db" => "coach_name", "dt" => 5),
                        array("db" => "id", "dt" => 6,"formatter" => function($d, $row){
                            $traineeScheduleId = DB::cell('tbl_trainee_schedule_header', 'id', $d)[0]['trainee_id'];
                            return "<a class='btn-show-edit-form' data-trainee-schedule-detail-id='$d' data-trainee-id='$traineeScheduleId' href='javascript:void(0)'>Edit Coach</a>";
                        } )
                    )), 200);
                } else {
                    throw new Exception("no data collected");
                }


            } catch(Exception $e){
                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(
                    array(
                        "response" => array(
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => $e->getMessage()
                        )
                    ), 400
                );
            }

        });


        $app->map(["GET"], '/details/unassigned/[{id}]', function($request, $response, $arguments){
            require "spp.class.php";
            $bearer = new Access;
            $token = $bearer->getBearerToken();
            $method = $request->getMethod();
            $attribute = $request->getAttribute('id');
            $data = $request->getParsedBody() ? $request->getParsedBody() : NULL;
            $dbCredentials = array
            (
                "user" => INI::get('username'),
                'pass' => INI::get('password'),
                'db' => INI::get('database'),
                'host' => INI::get('servername')
            );
            try {
                $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));

                DB::deletTableRows('tmp_unassigned_trainee_schedule_detail');

                if(DB::unsignedCoaches()){
                return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(ssp::simple($_GET, $dbCredentials, 'tmp_unassigned_trainee_schedule_detail', 'id', array(
                        array("db" => "id", "dt" => 0),
                        array("db" => "trainee_name", "dt" => 1),
                        array("db" => "class_date", "dt" => 2, "formatter" => function($d, $row){
                            return date('m/j/Y', strtotime($d));
                        }),
                        array("db" => "start_time", "dt" => 3,"formatter" => function($d, $row){
                            return date('G:i', strtotime($d));
                        }),
                        array("db" => "lesson_name", "dt" => 4),
                        array("db" => "id", "dt" => 5,"formatter" => function($d, $row){
                            $traineeScheduleId = DB::cell('tbl_trainee_schedule_header', 'id', $d)[0]['trainee_id'];
                            return "<a class='btn-show-assign-form' data-trainee-schedule-detail-id='$d' data-trainee-id='$traineeScheduleId' href='javascript:void(0)'>Assign Coach</a>";
                        } )




                    )), 200);
                } else {
                    throw new Exception("no data collected");
                }


            } catch(Exception $e){
                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(
                    array(
                        "response" => array(
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => $e->getMessage()
                        )
                    ), 400
                );
            }

        });
    });
});
