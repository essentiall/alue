<?php

namespace ALUE\Database;

class Connection
{
    public function __construct() {
        // database object
        $this->db = require('database-connection.php');
    }

    // database connection
     private function connection()
    {
        if ($this->db) {
            return $this->db;
        }
        return NULL;
    }

        public function column_check($tableName = STRING, $column) {
            if ($database = $this->db) {

                $col = $database->query("SHOW COLUMNS FROM $tableName LIKE '%$column'");
                if ($col->num_rows) {
                    return true;
                } else { return false; }
            }
        }

        public function alter($proc = NULL, $table = NULL, $fields = NULL, $whereTo = NULL) {

        // initializing database connection
        if ($database = $this->connection()) {

            // possible values for proc argument
            $procedure = array(
                "add"       =>  "ADD",
                "rename"    =>  "RENAME COLUMN",
                "delete"    =>  "DROP COLUMN"
            );

            // count the number of field's that will be added
            $num = gettype($fields) !== 'NULL' ? count($fields) : $fields;

            // procedure of sql statement
            $procedure = $procedure[$proc];


            // a placeholder where the new row will be added right after the column name
            if ( $whereTo !== NULL || 'NULL' ) {

                if ($procedure == "add" || "rename") {
                    // loop through fields
                    foreach($fields as $field => $value ) {
                        $database->query("ALTER TABLE $table $procedure $field $value");
                        echo $field . ' => ' . $value . "\n";
                    }
                    // closing databse connection;
                }
                $database->close();
            }
        }
    }

    // Fetch single row
    public function row($tbl = STRING, $field = STRING, $rowName = STRING ) {
        if ($database = $this->connection()) {
            
            if ($tbl === 'tbl_coach_schedule') {
                if ($table = $database->query("SELECT *, 
                DATE_FORMAT(schedule_date, '%m/%e/%Y') AS 'schedule_date',
                DATE_FORMAT(schedule_start_time, '%k:%i') AS 'schedule_start_time',
                DATE_FORMAT(schedule_end_time, '%k:%i') AS 'schedule_end_time'
                
                FROM 
                
                $tbl WHERE `$field` = '$rowName'")) {
                    $row = $table->num_rows;
                    if ( $row > 0 ) {
                        while ($rows = $table->fetch_assoc()) {
                            $data[] = $rows;
                        }
                        return $data;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            if ($tbl === 'tbl_trainee') {
                if ($table = $database->query("SELECT *, 
                DATE_FORMAT(study_period_start_date, '%m/%e/%Y') AS 'study_period_start_date',
                 DATE_FORMAT(study_period_end_date, '%m/%e/%Y') AS 'study_period_end_date'
                
                FROM 
                
                $tbl WHERE `$field` = '$rowName'")) {
                    $row = $table->num_rows;
                    if ( $row > 0 ) {
                        while ($rows = $table->fetch_assoc()) {
                            $data[] = $rows;
                        }
                        return $data;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            if ($table = $database->query("SELECT * FROM $tbl WHERE `$field` = '$rowName'")) {
                $row = $table->num_rows;
                if ( $row > 0 ) {
                    
                    while ($rows = $table->fetch_assoc()) {
                        $data = $rows;
                    }
                    return $data;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    // Validate tables
    public function describe($table = STRING) {
        if ($this->connection()->query("DESCRIBE $table")) {
            return true;
        } return false;
    }

    public function insert($tableName = STRING, $data = array() ) {
        if ($database = $this->connection()) {
            $fields = "";
            $fieldsValue = "";
            $count = count($data);
            $counter = 0;
            foreach ($data as $key => $val) {
                $counter += 1;
                if ($counter == $count) {
                    $fields .= "$key ";
                    $fieldsValue .= "'$val' ";
                    continue;
                }
                if (gettype($val) == 'integer') {
                    $fieldsValue .= "$val, ";
                    continue;
                }
                $fields .= "$key, ";
                $fieldsValue .= "'$val', ";


            } $queryString = "INSERT IGNORE INTO `$tableName` (" . $fields . ") VALUES(" . $fieldsValue . ")";
            $database->query($queryString);
            

        }
    }
    /* table create function */
    public function create($table = NULL, $fields = NULL) {

        // number of fields to be created as counter
        $num = count($fields);

        // empty variable for placeholder
        $fieldEntry = '';

        if ( $database = $this->connection()) {
            foreach ($fields as $field => $value) {
                $fieldCounter[] = $value;
                if ( count($fieldCounter) == $num )
                {
                    $fieldEntry .= $field . ' ' . $value;
                    continue;
                } $fieldEntry .= $field  . " $value, ";
            }

            if ( $database->query("DESCRIBE $table")) {
                // check and see if table already exists

                return json_encode(array( "data" => array( "status" => "302", "message" => "table exists [$table]")));

            }
            // generate / create table
            $database->query("CREATE TABLE IF NOT EXISTS `$table` ($fieldEntry)");

            // closing database connection


            // return response status
            return json_encode(array( "data" => array( "status" => "200", "message" => "table successfully created [$table]")));
        }

    }

    public function update($tableName = STRING, $data = array(), $index = STRING,$keys = STRING) {

        if ($database = $this->connection()) {
            if (is_array($data)) {
                foreach($data as $key => $val) {
                    $database->query("UPDATE $tableName SET $key = '$val' WHERE $keys LIKE '$index' OR $keys = '$index'");
                }
            } else {
                $database->query("UPDATE $tableName SET $keys = '$data' WHERE $keys LIKE '$index' OR $keys = '$index'");
            }
            

        }
    }

    public function deleteRow($tableName = STRING, $index = STRING, $keys = STRING) {

        if ($database = $this->connection()) {
            $database->query("DELETE FROM $tableName WHERE $keys LIKE '$index' OR  $keys = '$index'");
        }
    }

    /* delete table function
        WARNING!
        * be careful on using this function as it will delete the table including all the field's assigned to it
    */
    public function delete($table = NULL)
    {
        if ( $database = $this->connection()) {

            // check table if it does exists
            if($database->query("DESCRIBE `$table`")) {

                // execute mysql query
                $database->query("DROP TABLE `$table`");

                // close database connection
                $database->close();

                // return a success response
                return json_encode(array( "data" => array( "status" => "200", "message" => "table successfully deleted [$table]")));

            }
            // return a unsuccessful response
            return json_encode(array( "data" => array( "status" => "302", "message" => "table does not exists [$table]")));
        }
    }


}