<?php
 // .ini file that contain your database settings
 $iniFile =   '../app/config.ini';

 if ($config = parse_ini_file($iniFile, true))
 {
     // credentials from your .ini file
     $config = $config['local'];
     $user = $config['username'];
     $pass = $config['password'];
     $server = $config['servername'];
     $dbName = $config['database'];

    // create an instance of database object which you can access your data
    if ($mySQL = new mysqli($server,$user,$pass,$dbName)) {
        return $mySQL;
    }
 }