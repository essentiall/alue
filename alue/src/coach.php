<?php
use \Firebase\JWT\JWT;
use \ALUE\Token\Access as Access;
use \ALUE\Database\Connection as Connection;
use \ALUE\Method\Handler as Handler;
use \ALUE\INI\INI as INI;
use \ALUE\DB\DB as DB;


$app->map(['GET', 'POST', (string)"DELETE", 'PUT'], '/coach/[{id}]', function($request, $response, $args){
require 'spp.class.php';
$token = new Access;
$database = new Connection;
$param = $request->getAttribute('id');
$method = $request->getMethod();
$bearer = $token->getBearerToken();
$parser = $request->getParsedBody() ? $request->getParsedBody() : NULL;
$dbCredentials = array
    (
        "user" => INI::get('username'),
        'pass' => INI::get('password'),
        'db' => INI::get('database'),
        'host' => INI::get('servername')
    );
try {
    $data = array();
    $tokenAccess = JWT::decode($bearer, ALUE_KEY , array('HS256'));
    if ($method === "GET") {
        if ($param === 'get') {
            return DB::getAllRows('tbl_coach', $response);
            exit;
        }
        return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(ssp::simple($_GET, $dbCredentials, 'tbl_coach', 'id', array
                (
                    array( "db" => "id", "dt" => 0 ),
                    array("db" => "first_name", "dt" => 1),
                    array("db" => "last_name", "dt" => 2),
                    array("db" =>"email", "dt" => 3),
                    array("db" => "contact_num", "dt" => 4),
                    array("db" => "type", "dt" => 5),
                    array("db" => "id", "dt" => 6,  "formatter" => function($d, $row ) {
                        return '<a class="btn-show-view-schedule-form" data-id="' . $d . '" href="javascript:void(0);">View Schedule</a> | <a class="btn-show-edit-form" data-id="' . $d . '" href="javascript:void(0);">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a> | <a class="btn-show-change-passsword-form" data-id="' .$d.'" href="javascript:;">Change Password</a>';
                    }),
                )), 200 );
    }
    if ($method === (string)"DELETE" ) {
        return Handler::method($method, $request, $response, $data, 'tbl_coach');
    }
    if (is_array($parser)) {
        foreach ($parser as $key => $value ) {
                if ($database->row('tbl_coach', 'email', $value) && $method == 'POST') {
                    return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(array
                        (
                            "response" => array
                            (
                                "status" => 400,
                                "message" => "Not Acceptable",
                                "error" => "duplicated record"
                            )
                        ), 400
                    );
                    break;
                }
            if ($key == 'password') {
                $data[$key] = password_hash($value, PASSWORD_BCRYPT);
                continue;
            }
            $data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
        }
        return Handler::method($method, $request, $response, $data, 'tbl_coach');
    } else { throw new Exception("no data or null key's"); }
} catch(Exception $e) {
    // Expired token or ivalid Key
    if( $e->getMessage() == "no data or null key's") {
        $message = "Bad Request";
        $status = 400;
    } else {
        $message = "Unauthorized";
        $status = 401;
    }
    return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(array
            (
            "response" => array(
            "status" => $status,
            "message" => $message,
            "error" => $e->getMessage()
            )

        ), 401
    );
}

});