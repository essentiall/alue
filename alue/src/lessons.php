<?php

use \Firebase\JWT\JWT as JWT;
use \ALUE\Token\Access as Access;
use \ALUE\Method\Handler as Handler;
use \ALUE\Data\Table as Table;
use \ALUE\Database\Connection as Connection;
use \ALUE\DB\DB;
use \ALUE\INI\INI as INI;

$app->map(['GET', 'POST', 'DELETE', 'PUT'], '/lesson/[{id}]', function($request, $response, $args) {
    require "spp.class.php";
$database = new Connection;
$bearer = new Access;
$token = $bearer->getBearerToken();
$param = $request->getAttribute('id');
$method = $request->getMethod();
$keys = $request->getParsedBody() ? $request->getParsedBody() : NULL;
$dbCredentials = array
    (
        "user" => INI::get('username'),
        'pass' => INI::get('password'),
        'db' => INI::get('database'),
        'host' => INI::get('servername')
    );
    try {
        $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));


        $data = array();
        if ($method === 'GET' ) {
            if ($param === 'get') {
                return DB::getAllRows('tbl_lessons', $response);
                exit;
            }
            return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson(ssp::simple($_GET, $dbCredentials, 'tbl_lessons', 'id', array
        (
            array("db" => "id", "dt" => 0 ),
            array("db" => "lesson_name", "dt" => 1),
            array("db" => "fixed_flg", "dt" => 2),
            array("db" => "coach_assigned_flg", "dt" => 3),
            array("db" => "id", "dt" => 4, "formatter" => function($d, $row ) {
                return '<a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a>';
            }),
        )), 200 );

        }
        if ($method === 'DELETE') {
            return Handler::method($method,$request, $response, $data, 'tbl_lessons');
        }
        if (is_array($keys)) {
            foreach($keys as $key => $value) {
                if ($database->row('tbl_lessons', 'lesson_name', $value) && $method == 'POST') {
                    return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(array
                        (
                            "response" => array
                            (
                                "status" => 400,
                                "message" => "Not Acceptable",
                                "error" => "duplicated record"
                            )
                        ), 400
                    );
                    break;
                }
                $data[$key] =  filter_var($value, FILTER_SANITIZE_STRING );
            }
            return Handler::method($method,$request, $response, $data, 'tbl_lessons');
        } else {
            throw new Exception("no data or null key's");
        }
    } catch(Exception $e) {

        if( $e->getMessage() == "no data or null key's") {
            $message = "Bad Request";
            $status = 400;
        } else {
            $message = "Unauthorized";
            $status = 401;
        }
        return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson(
            array(
                "response" => array(
                    "status" => $status,
                    "message" => $message,
                    "error" => $e->getMessage()
                )
            ), $status
        );
    }
});
