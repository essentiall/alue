<?php

use \ALUE\DB\DB;
use \Firebase\JWT\JWT as JWT;
use \ALUE\Token\Access as Access;

$app->group('/schedule', function() use($app){


    $app->map(["GET", "PUT"], '/[{id}]', function($request, $response, $args){

        $bearer = new Access;
        $token = $bearer->getBearerToken();
         
                
        $param = explode("," ,$request->getAttribute('id'));
        try {
            $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
            
            if ($data = DB::schedule($param)) {
                return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson($data, 200 );
            } else {
                throw new Exception("no data found.");
            }
        } catch(Exception $e) {
            return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(array( "response" => array(
                        
                        "status" => 400,
                        "message" => "Bad Request",
                        "error" => $e->getMessage())), 400);
        }

    });
    
});

$app->group('/class-counter', function() use($app){
    

    $app->map(["GET"], '/month[/{month}[/{year}]]', function($request, $response, $arguments){
        $month = $request->getAttribute('month');
        $year = $request->getAttribute('year');
        $bearer = new Access;
    $token = $bearer->getBearerToken();

        try {
            $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
            $counter = DB::counter($month, $year);

            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson( $counter, 200 );

        } catch(Exception $e) {
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(array( "response" => array(
                
                "status" => 400,
                "message" => "Bad Request",
                "error" => $e->getMessage())), 400);


        }
       







    });
});