<?php

use \Firebase\JWT\JWT;
use \ALUE\Database\Connection as Connection;
use \ALUE\DB\DB;
use \ALUE\Method\Handler as Handler;
use \ALUE\Token\Access as Access;
use \ALUE\INI\INI as INI;

// ENABLING CORS AND SETTING REQUEST HEADERS
header('Access-Control-Allow-Methods: PUT, DELETE, GET, POST');
header('Access-Control-Allow-Headers: Authorization');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');

$app->map(['GET', 'POST', 'DELETE', 'PUT'], '/curriculum/[{id}]', function( $request, $response, $args ) {
    require "spp.class.php";
    $database = new Connection;
    $param = $request->getAttribute('id');
    $auth = new Access;
    $token = $auth->getBearerToken();
    $parser = $request->getParsedBody() ? $request->getParsedBody() : NULL;
    $method = $request->getMethod();
    
    $dbCredentials = array
    (
        "user" => INI::get('username'),
        'pass' => INI::get('password'),
        'db' => INI::get('database'),
        'host' => INI::get('servername')
    );
   
    try {
        $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
        if ($method === 'GET') {

            if ($param === 'header') {
                if (isset($_GET['get']) && $_GET['get'] === 'all') {
                    return DB::getAllRows('tbl_curriculum_set_header', $response);
                    exit;
                }

                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(ssp::simple($_GET, $dbCredentials, 'tbl_curriculum_set_header', 'curriculum_id', array
                (
                    array("db" => "curriculum_id", "dt" => 0 ),
                    array("db" => "curriculum_name", "dt" => 1),
                    array("db" => "weeks_num", "dt" => 2),
                    array("db" => "assessment_lvl", "dt" => 3),
                    array("db" => "curriculum_id", "dt" => 4, "formatter" => function($d, $row ) {
                        return '<a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Add</a> | <a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a>';
                    }),
                )), 200 );
            }

            if ($param === 'detail') {
                if (isset($_GET['get']) && $_GET['get'] === 'all') {
                    return DB::getAllRows('tbl_curriculum_set_detail', $response);
                    exit;
                }

                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(ssp::simple($_GET, $dbCredentials, 'tbl_curriculum_set_detail', 'id', array
                (
                    array("db" => "id", "dt" => 0 ),
                    array("db" => "curriculum_id", "dt" => 1 ),
                    array("db" => "lesson_id", "dt" => 2),
                    array("db" => "session_num", "dt" => 3),
                    array("db" => "id", "dt" => 4, "formatter" => function($d, $row ) {
                        return '<a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Add</a> | <a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a>';
                    }),
                )), 200 );
            }

        }
        if ($method === 'DELETE') {
            $data = array();
            if( isset($_GET['ent']) && $_GET['ent'] === 'header') {

                return Handler::method($method, $request, $response, $data, 'tbl_curriculum_set_header');
            } 
            if( isset($_GET['ent']) && $_GET['ent'] === 'detail') {
                return Handler::method($method,$request, $response, $data, 'tbl_curriculum_set_detail');
            }
        }
        if (is_array($parser)) {
            
            if ( isset($_GET['ent']) && $_GET['ent'] === 'header' && $method === 'PUT') {
                foreach ($parser as $key => $value ) {
                   
                    if ($database->row('tbl_curriculum_set_header', 'curriculum_id', $value ) && $method === 'PUT') {
                        return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(array
                            (
                                "response" => array
                                (
                                    "status" => 400,
                                    "message" => "Not Acceptable",
                                    "error" => "duplicated record"
                                )
                            ), 400
                        );
                        break;  
                    }
                    $data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
                }
                    return Handler::method($method, $request, $response, $data, 'tbl_curriculum_set_header');
            }
            if ($param === 'header') {
                foreach ($parser as $key => $value ) {
                    if ($database->row('tbl_curriculum_set_header', 'curriculum_id', $value) && $method == 'POST') {
                        return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(array
                            (
                                "response" => array
                                (
                                    "status" => 400,
                                    "message" => "Not Acceptable",
                                    "error" => "duplicated record"
                                )
                            ), 400
                        );
                        break;
                    }
                $data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
            }
            return Handler::method($method, $request, $response, $data, 'tbl_curriculum_set_header');

            } if ($param === 'detail') {
                foreach ($parser as $key => $value ) {
                    if ($database->row('tbl_curriculum_set_detail', 'curriculum_id', $value) && $method == 'POST') {
                        return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(array
                            (
                                "response" => array
                                (
                                    "status" => 400,
                                    "message" => "Not Acceptable",
                                    "error" => "duplicated record"
                                )
                            ), 400
                        );
                        break;
                    }
                $data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
            }
            return Handler::method($method, $request, $response, $data, 'tbl_curriculum_set_detail');
            }
            
        }

    } catch(Exception $e) {
        return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(array
            (
            "response" => array(
            "status" => 401,
            "message" => "Unauthorized",
            "error" => $e->getMessage()
            )

        ), 401
    );


    }

});