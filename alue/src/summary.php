<?php

// SET TIMEZONE DEFAULT (PH/HK/SG)
date_default_timezone_set('Asia/Kuala_Lumpur');

use \Firebase\JWT\JWT;
use \ALUE\Method\Handler;
use \ALUE\Token\Access as Access;
use \ALUE\Database\Connection as Connection;
use \Psr\Http\Message\StreamInterface;
use \ALUE\DB\DB;
use \ALUE\INI\INI as INI;

$app->group('/summary', function() use($app){



    $app->map(['GET', 'POST'], '/daily/[{id}]', function($request, $response, $args){
    $bearer = new Access;
    $token = $bearer->getBearerToken();
    $method = $request->getMethod();
    $param = ! empty($request->getAttribute('id')) ? $request->getAttribute('id') : null;
        try {
        $payload = JWT::decode($token, ALUE_KEY, array('HS256'));
            if ($method === 'GET') {
                /* DEFAULT */

                return DB::summary('tbl_trainee_schedule_header', $_GET, $response, $param );


                /* DEFAULT */
            }
        } catch(Exception $e) {
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(array(
                "status" => 401,
                "message" => "Unauthorized",
                "error" => $e->getMessage()
            ), 401);
        }
    });
});
