<?php

use \Firebase\JWT\JWT as JWT;
use \ALUE\Token\Access as Access;
use \ALUE\Method\Handler as Handler;
use \ALUE\Data\Table as Table;
use \ALUE\INI\INI as INI;
use \ALUE\DB\DB as DB;

$app->map(['GET', 'POST', 'DELETE', 'PUT'], '/company/[{id}]', function($request, $response, $args) {
require "spp.class.php";
$bearer = new Access;
$token = $bearer->getBearerToken();
$param = $request->getAttribute('id');
$method = $request->getMethod();
$keys = $request->getParsedBody() ? $request->getParsedBody() : NULL;
$dbCredentials = array
    (
        "user" => INI::get('username'),
        'pass' => INI::get('password'),
        'db' => INI::get('database'),
        'host' => INI::get('servername')
    );
    try {
        $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
        $data = array();
        if ($method === 'GET' ) {

        if ($param === "get") {

            return DB::getAllRows('tbl_company', $response);
        }
        return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson(ssp::simple($_GET, $dbCredentials, 'tbl_company', 'id', array
        (
            array("db" => "id", "dt" => 0 ),
            array("db" => "company", "dt" => 1),
            array("db" => "complete_name", "dt" => 2),
            array("db" => "industry", "dt" => 3),
            array("db" => "id", "dt" => 4, "formatter" => function($d, $row ) {
                return '<a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:;">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a>';
            }),
        )), 200 );
        }
        if ($method === 'DELETE') {
            return Handler::method($method,$request, $response, $data, 'tbl_company');
        }
        if (is_array($keys)) {
            foreach($keys as $key => $value) {
                $data[$key] =  filter_var($value, FILTER_SANITIZE_STRING );
            }
            return Handler::method($method,$request, $response, $data, 'tbl_company');
        } else {
            throw new Exception("no data or null key's");
        }
    } catch(Exception $e) {

        if( $e->getMessage() == "no data or null key's") {
            $message = "Bad Request";
            $status = 400;
        } else {
            $message = "Unauthorized";
            $status = 401;
        }
        return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson(
            array(
                "response" => array(
                    "status" => $status,
                    "message" => $message,
                    "error" => $e->getMessage()
                )
            ), $status
        );
    }
});
