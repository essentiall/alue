<?php

date_default_timezone_set('Asia/Kuala_Lumpur');

use \Firebase\JWT\JWT as JWT;
use \ALUE\Token\Access as Access;
use \ALUE\Method\Handler as Handler;
use \ALUE\DB\DB as DB;
use \ALUE\Data\Table as Table;
use \ALUE\Database\Connection as Connection;
use \ALUE\INI\INI as INI;

$app->map(['GET', 'POST', 'DELETE', 'PUT'], '/coach-schedule/[{id}]', function($request, $response, $args) {

require "spp.class.php";
$database = new Connection;
$bearer = new Access;
$token = $bearer->getBearerToken();
$ignore = isset($_GET['ignore']) ? true : false;
$param = $request->getAttribute('id');
$filter = isset($_GET['filter']) && !empty($_GET['filter']) ? $_GET['filter'] : false;
$key = isset($_GET['key']) && !empty($_GET['key']) ? $_GET['key'] : false;
$method = $request->getMethod();
$keys = $request->getParsedBody() ? $request->getParsedBody() : NULL;

$dbCredentials = array
    (
        "user" => INI::get('username'),
        'pass' => INI::get('password'),
        'db' => INI::get('database'),
        'host' => INI::get('servername')
    );
    try {
        $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
        $data = array();
        if ($method === 'GET' ) {
            if ($param === 'get') {
                return DB::getAllRows('tbl_coach_schedule', $response,'tbl_coach_schedule');
                exit;
            }
            if($filter) {
                $fields = DB::filter('tbl_coach_schedule', $filter, $key, $response);
                DB::deletTableRows('tmp_coach_schedule');
                for($i=0;$i < count($fields); $i++) {
                     DB::insertRecord('tmp_coach_schedule',$fields[$i], $filter, $key);
                }

                return $response

                ->withHeader("Content-Type", "application/json")
                ->withJson(ssp::simple($_GET, $dbCredentials,'tmp_coach_schedule', 'id', array
                (
                    array("db" => "id", "dt" => 0 ),
                    array("db" => "coach_id", "dt" => 1, "formatter" => function($d, $row){
                    $data = DB::joiner('tbl_coach_schedule', 'tbl_coach');
                        foreach($data as $key => $value) {
                                if( (integer)$data[$key]['coach_id'] === (integer)$d) {
                                    $name = $data[$key]['first_name'] . " " . $data[$key]['last_name'];
                                    break;
                                }
                        } return $name;
                    }),
                    array("db" => "schedule_date", "dt" => 2, "formatter" => function($d, $row){
                        return date('m-d-Y', strtotime($d));
                    }),
                    array("db" => "schedule_start_time", "dt" => 3, "formatter" => function($d, $row){
                        return date('G:i', strtotime($d)); }),
                    array("db" =>"schedule_end_time", "dt" => 4, "formatter" => function($d, $row){
                        return date('G:i', strtotime($d)); }),
                    array("db" => "login_flg", "dt" => 5),
                    array("db" => "absent_flg", "dt" => 6),
                    array("db" => "id", "dt" => 7,  "formatter" => function($d, $row ) {
                    return '<a class="btn-show-absent-form" data-id="'.$d.'" href="javascript:void(0);">Absent</a> | <a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:void(0);">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:void(0);">Delete</a>';
                    }),
                )), 200 );
            } else {

        return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson(ssp::simple($_GET, $dbCredentials,'tbl_coach_schedule', 'id', array
        (
            array("db" => "id", "dt" => 0 ),
            array("db" => "coach_id", "dt" => 1, "formatter" => function($d, $row){
            $data = DB::joiner('tbl_coach_schedule', 'tbl_coach');
                foreach($data as $key => $value) {
                         if( (integer)$data[$key]['coach_id'] === (integer)$d) {
                            $name = $data[$key]['first_name'] . " " . $data[$key]['last_name'];
                            break;
                         }
                } return $name;
            }),
            array("db" => "schedule_date", "dt" => 2, "formatter" => function($d, $row){
                return date('m-d-Y', strtotime($d));
            }),
            array("db" => "schedule_start_time", "dt" => 3, "formatter" => function($d, $row){
                return date('G:i', strtotime($d)); }),
            array("db" =>"schedule_end_time", "dt" => 4, "formatter" => function($d, $row){
                return date('G:i', strtotime($d)); }),
            array("db" => "login_flg", "dt" => 5),
            array("db" => "absent_flg", "dt" => 6),
            array("db" => "id", "dt" => 7,  "formatter" => function($d, $row ) {
             return '<a class="btn-show-absent-form" data-id="'.$d.'" href="javascript:void(0);">Absent</a> | <a class="btn-show-edit-form" data-id="'.$d.'" href="javascript:void(0);">Edit</a> | <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:void(0);">Delete</a>';
            }),
        )), 200 );
         }
    }
        if ($method === 'DELETE') {
            return Handler::method($method,$request, $response, $data, 'tbl_coach_schedule');
        }
        if (is_array($keys)) {
            $format = isset($keys["schedule_date"]) ? $keys["schedule_date"] : NULL;
            $date = date('Y-m-d', strtotime($format));
            $id = isset( $keys['coach_id'] ) ? $keys['coach_id'] : NULL;


            if ($check = DB::checkDate( $date, $id, $method, $param)) {

                throw new Exception("date has been assigned already for coach id: $id"); 

            } else {
              foreach($keys as $key => $value) {
                  if ($key === 'schedule_date') {
                      $keys[$key] = date('Y-m-d H:i:s', strtotime($value));
                      if ($keys[$key] < date('Y-m-d H:i:s', time())) {
                          throw new Exception("invalid date. date must not be empty or lower than the current date.");
                      }
                      break;
                  }
                  if ($key === 'schedule_start_time') {
                      $keys[$key] = date('Y-m-d H:i:s', strtotime($value));
                      $startTime = date('Y-m-d H:i:s',strtotime($keys[$key]));
                      continue;
                  }
                  if ($key === 'schedule_end_time') {
                      $keys[$key] = date('Y-m-d H:i:s', strtotime($value));
                      $endTime = date('Y-m-d H:i:s',strtotime((string)$keys[$key]));
                      continue;
                  }

                  if ( isset($endTime, $startTime) && $endTime < $startTime) {
                      return $response
                              ->withHeader("Content-Type", "application/json")
                              ->withJson(array
                              (
                                  "response" => array
                                  (
                                      "status" => 400,
                                      "message" => "Bad Request",
                                      "error" => "End-Time[$endTime] must not be less than Start-Time[$startTime].",
                                  )
                              ), 400 );
                              exit;
                  }
                  $keys[$key] =  filter_var($value, FILTER_SANITIZE_STRING );
              }
            return Handler::method($method,$request, $response, $keys, 'tbl_coach_schedule');
          }
        } else {
            throw new Exception("no data or null key's");
        }
    } catch(Exception $e) {


        if( $e->getMessage() == "no data or null key's") {
            $message = "Bad Request";
            $status = 400;
        }
        else if( $e->getMessage() == "date has been assigned already for coach id: " . $keys['coach_id']) {
          $message = "Bad Request";
          $status = 400;
        }
        else if($e->getMessage() == "invalid date. date must not be empty or lower than the current date.") {
            $message = "Bad Request";
            $status = 400;
        }
        else if($e->getMessage() == "date already exists.") {
            $message = "Bad Request";
            $status = 400;
        }
        else {
            $message = "Unauthorized";
            $status = 401;
        }
        return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson(
            array(
                "response" => array(
                    "status" => $status,
                    "message" => $message,
                    "error" => $e->getMessage()
                )
            ), $status
        );
    }
});
