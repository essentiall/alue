<?php
    use \ALUE\Database\Connection As Connection;

    $app->get('/create', function($request, $response){

        $database = new Connection;
        // Check if the table is already created
        if (! $database->describe("tbl_configuration")) {
            $tblAdmin = array
            (
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "username"                  => "VARCHAR(255) NOT NULL",
                "password"                  => "VARCHAR(255) NOT NULL",
                "user_level"                => "VARCHAR(255)"
            );  
            // Settings
            $tblSettings = array
            (
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "configuration"             => "TEXT(255)",
                "setting"                   => "TINYINT(1)"
            );

            // Coach
            $tblCoach = array
            (
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "last_name"                 => "VARCHAR(255) NOT NULL",
                "first_name"                => "VARCHAR(255) NOT NULL",
                "contact_num"               => "VARCHAR(20)",
                "email"                     => "VARCHAR(255) NOT NULL",
                "password"                  => "TEXT(255)",
                "type"                      => "VARCHAR(2)",
                "offline_coach_flg"         => "TINYINT(1)",
                "prioritize_schedule_flg"   => "TINYINT(1)"
            );

            // Coach Schedule
            $tblCoachSchedule = array
            (
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "coach_id"                  => "INT(50)",
                "schedule_date"             => "DATE",
                "schedule_start_time"       => "TIME",
                "schedule_end_time"         => "TIME",
                "login_flg"                 => "TINYINT(1)",
                "absent_flg"                => "TINYINT(1)"
            );

            $hstCoachSchedule = array
            (
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "coach_id"                  => "INT(50)",
                "schedule_date"             => "DATE",
                "schedule_start_time"       => "TIME",
                "schedule_end_time"         => "TIME",
                "login_flg"                 => "TINYINT(1)",
                "absent_flg"                => "TINYINT(1)"
            );

            // TMP Coach Schedule
            $tmpCoachSchedule = array
            (
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "coach_id"                  => "INT(50)",
                "schedule_date"             => "DATE",
                "schedule_start_time"       => "TIME",
                "schedule_end_time"         => "TIME",
                "login_flg"                 => "TINYINT(1)",
                "absent_flg"                => "TINYINT(1)"
            );

            // Company
            $tblCompany = array(
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "company"                   => "VARCHAR(30)",
                "complete_name"             => "VARCHAR(255)",
                "industry"                  => "VARCHAR(100)"
            );

            // Trainee
            $tblTrainee = array
            (
                "trainee_id"                => "VARCHAR(50) NOT NULL PRIMARY KEY",
                "last_name"                 => "VARCHAR(50)",
                "first_name"                => "VARCHAR(50)",
                "company_id"                => "INT(11)",
                "coach_id_incharge"         => "INT(11)",
                "num_weeks"                 => "SMALLINT(3)",
                "study_period_start_date"   => "DATE",
                "study_period_end_date"     => "DATE",
                "type"                      => "VARCHAR(50)",
                "initial_assessment_lvl"    => "VARCHAR(2)",
            );

            // Lessons
            $tblLessons = array(
                "id"                        =>  "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "lesson_name"               =>  "VARCHAR(255)",
                "fixed_flg"                 =>  "TINYINT(1)",
                "coach_assigned_flg"        =>  "TINYINT(1)",
                "cic_flg"                   =>  "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0'"
            );

            $tblCurriculumSetHeader = array(
                "curriculum_id"             => "VARCHAR(10) PRIMARY KEY",
                "curriculum_name"           => "VARCHAR(255)",
                "total_weeks_num"                 => "INT",
                "assesstment_lvl"            => "INT",
            );

            $tblCurriculumSetDetail = array(
                "id" => "INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "curriculum_id" => "VARCHAR(10)",
                "lesson_id" => "INT",
                "week_num" => "INT",
                "day_num" => "INT",
                "sort_num" => "INT",
            );

            $tblCurriculumSetDefaultTimeLessons = array(
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "curriculum_id"             => "VARCHAR(10)",
                "lesson_id"                 => "INT",
                "generated_flg"             => "BOOLEAN",
            );

            $tbTraineeScheduleHeader = array(
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT",
                "curriculum_id"             => "VARCHAR(10) NOT NULL",
                "trainee_id"                => "VARCHAR(50), CONSTRAINT PK_trainee PRIMARY KEY (id, curriculum_id)",
                "generated_flg"             => "BOOLEAN",
                
            );
            $tblTraineeScheduleDetail = array(
                "id"                        => "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
                "trainee_schedule_id"       => "VARCHAR(50)",
                "coach_id"                  => "INT",
                "lesson_id"                 => "INT",
                "class_date"                => "DATE",
                "class_start_time"          => "TIME",
                "class_end_time"            => "TIME",
                "remarks"                   => "VARCHAR(100) NOT NULL DEFAULT '\"\"'" 
            );

            $tmpAssignedTraineeScheduleDetail = array(
                "id"                        => "INT(11)  PRIMARY KEY",
                "trainee_name"              => "VARCHAR(50)",
                "class_date"                => "DATE",
                "start_time"                => "TIME",
                "lesson_name"               => "VARCHAR(100)",
                "coach_name"                => "VARCHAR(50)",
                "remarks"                   => "VARCHAR(100) NOT NULL DEFAULT '\"\"'"
            );

            $tmpUnassignedTraineeScheduleDetail = array(
                "id"                        => "INT(11)  PRIMARY KEY",
                "trainee_name"              => "VARCHAR(50)",
                "class_date"                => "DATE",
                "start_time"                => "TIME",
                "lesson_name"               => "VARCHAR(100)",
                "coach_name"                => "VARCHAR(50)",
                "remarks"                   => "VARCHAR(100) NOT NULL DEFAULT '\"\"'"
            );

            // DATA Table
            $ALUETable = array(
                "admin"                                         => $tblAdmin,
                "tbl_curriculum_set_default_time_lessons"       => $tblCurriculumSetDefaultTimeLessons,
                "tbl_trainee_schedule_detail"                   => $tblTraineeScheduleDetail,
                "tmp_assigned_trainee_schedule_detail"          => $tmpAssignedTraineeScheduleDetail,
                "tmp_unassigned_trainee_schedule_detail"        => $tmpUnassignedTraineeScheduleDetail,
                "tbl_trainee_schedule_header"                   => $tbTraineeScheduleHeader,
                "tbl_configuration"                             => $tblSettings,
                "tbl_coach"                                     => $tblCoach,
                "tbl_coach_schedule"                            => $tblCoachSchedule,
                "tmp_coach_schedule"                            => $tmpCoachSchedule,
                "coach_schedule_history"                        => $hstCoachSchedule,
                "tbl_company"                                   => $tblCompany,
                "tbl_trainee"                                   => $tblTrainee,
                "tbl_lessons"                                   => $tblLessons,
                "tbl_curriculum_set_header"                     => $tblCurriculumSetHeader,
                "tbl_curriculum_set_detail"                     => $tblCurriculumSetDetail,
                

            );

            // Initialize table creation
            foreach($ALUETable as $key => $val) {
                $database->create($key, $val);
            }

            // Table and Fields created
            return $response
                    ->withHeader("Content-Type", "application/json")
                    ->withJson(
                        array
                    (
                        "response"                  => array
                        (
                            "status"                => 200,
                            "message"               => "OK",
                            "reports"               => "Table system initialized"
                        )
                    ), 200
                );
        } else {
            // Avoid to run the table code again
            return $response
                    ->withRedirect(ALUE_HOST . '/', 301);
        }

    });