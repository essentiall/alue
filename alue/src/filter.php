<?php

use \Firebase\JWT\JWT as JWT;
use \ALUE\Token\Access as Access;
use \ALUE\Method\Handler as Handler;
use \ALUE\Database\Connection as Connection;
use \ALUE\DB\DB as DB;


$app->map(['GET'], '/filter/[{id}]', function($request, $response){
$method = $request->getMethod();
try {
    $database = new Connection;
    $param = filter_var($request->getAttribute('id'), FILTER_SANITIZE_STRING);
    $key = $_GET['key'];
    $index = $_GET['index'];
    $access = new Access;
    $token = $access->getBearerToken();
    $verify = JWT::decode($token, ALUE_KEY, array('HS256'));

    return $response
        ->withHeader("Content-Type", "application/json")
        ->withJson( array(
            "response" => array
                (
                    "status" => 200,
                    "message" => "OK",
                    "data" => DB::row($param, $key, $index)
                )
            ), 200);

} catch (Exception $e) {
    return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(array
            (
            "response" => array(
            "status" => 403,
            "message" => "Access Denied",
            "error" => $e->getMessage()
            )

        ), 401
    );
}



});