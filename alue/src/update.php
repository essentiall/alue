<?php
use \ALUE\DB\DB AS DB;
use \ALUE\Token\Access AS Access;
use \Firebase\JWT\JWT AS JWT;
use \ALUE\Error\Error;
use \ALUE\Error\Handler;

$app->group('/update', function() use($app){

    $app->map(["GET", "PUT", "DELETE"],'/[{id}]', function($request, $response, $arguments){
        $auth = new Access;
        $token = $auth->getBearerToken();
        $table = $request->getAttribute('id');
        try {
            $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
            $data = $request->getBody();
            $raw = json_decode($data->getContents(), true);
            if ( $fields = DB::updateData($table, $raw )) {

            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(array(
                    "response" => array(
                        "status" => 200,
                        "message" => 'OK',
                        "detail" => "data updated successfully"
                    )
                ), 200);
            } else {

                throw new Exception("failed to update data.");
            }


        } catch(Exception $e) {
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(array(
                    "response" => array(
                        "status" => 400,
                        "message" => 'Bad Request',
                        "error" => $e->getMessage()
                    )
                ), 400);


        }
    });
    $app->group('/schedules', function() use($app){
        $app->map(["GET", "PUT", "DELETE"], "/unassigned/[{id}]", function($request,$response){
            $auth = new Access;
            $token = $auth->getBearerToken();
            $P = (! empty($request->getAttribute('id'))) ? $request->getAttribute('id') : NULL;
            $V = (! empty($request->getParsedBody())) ? $request->getParsedBody() : NULL;
            try {
                $jwt = JWT::decode($token, ALUE_KEY, array('HS256'));
                $code = DB::unassigned($P, $V, $response);

                switch($code) {
                    case 0 :
                    throw new Handler("could not get field with #ID: $P", 400);
                    break;

                    case 1 :
                    throw new Handler(" coach #ID: has been set to 0 with field #id:  $P" , 400);
                    break;

                    case 2 :
                    throw new Handler("could not get coach with #ID: ". isset($V['coach_id']) ? $V['coach_id'] : "unknown id" , 400);
                    break;

                    case 3 :
                    throw new Handler("could not get trainee id." , 400 );
                    break;

                    case 4 :
                    throw new Handler("class date is out of range for coach, please specify another date." , 400 );
                    break;

                    case 5 :
                    throw new Handler("class date is out of range for trainee, please specify another date." , 400 );
                    break;

                    case 6 :
                    throw new Handler("selected time  is already allocated for trainee, please specify a different time." , 400 );
                    break;

                    case 7 :
                    throw new Handler("time is out of range for selected coach, please specify a different time." , 400 );
                    break;
                    
                    case 8 :
                    return $response->withHeader("Content-Type", "application/json")
                    ->withJson(array(
                      "response" => array(
                        "status" => 200,
                        "message" => "Ok",
                        "details" => "record updated successfully!"
                      )
                    ), 200 );
                    break;
                }
            

            

            } catch(Handler $e) {
            return $response
            ->withHeader("Content-Type", "application/json")
            ->withJson(array(
                    "response" => array(
                        "status" => $e->getCode(),
                        "message" => 'Bad Request',
                        "error" => $e->getMessage()
                    )
                ), $e->getCode());
            } catch(Exception $e) {
                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(array(
                        "response" => array(
                            "status" => 400,
                            "message" => 'Bad Request',
                            "error" => $e->getMessage()
                        )
                    ), 400);
                }
        });
    });    
});