<?php
//SET DEFAULT TIMEZONE
date_default_timezone_set('Asia/Kuala_Lumpur');

// ENABLING CORS AND SETTING REQUEST HEADERS
header('Access-Control-Allow-Methods: PUT, DELETE');
header('Access-Control-Allow-Headers: Authorization');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');

use \ALUE\Database\Connection as Connection;
use \ALUE\Method\Handler as Handler;
use \Firebase\JWT\JWT;
use \ALUE\Data\Table;


$app->post('/authenticate/{id}', function( $request, $response ){

    // Database object
    $database = new Connection;
    $procedure = $request->getAttribute('id');
    $method = $request->getMethod();
    // USERNAME & PASSWORD
    $admin = (isset($request->getParsedBody()['username']) ? filter_var($request->getParsedBody()['username'] , FILTER_SANITIZE_STRING) : "");
    $password = (isset($request->getParsedBody()['password']) ? filter_var($request->getParsedBody()['password'] , FILTER_SANITIZE_STRING) : "");
    $userLevel = (isset($request->getParsedBody()['user_level']) ? filter_var($request->getParsedBody()['user_level'] , FILTER_SANITIZE_STRING) : "");
    $queryTable = (isset($request->getParsedBody()['table']) ? filter_var($request->getParsedBody()['table'] , FILTER_SANITIZE_STRING) : "");

    // Access token
    $issuedAt = time();
    $serverName = gethostname();
    $notBefore = $issuedAt + 10;
    $expire = $notBefore + ( 3000 * 60 );
    $raw = array
    (
        "iat"                   => $issuedAt,
        "iss"                   => $serverName,
        "exp"                   => $expire,
        "uid"                   => $admin
    );
 
    
    if ( $database ) {
        return JWT::encode($raw, ALUE_KEY);
        // Authentication required to get or renew access token
        if ($procedure == 'get') {
            $user = $database->row($queryTable, 'username', $admin );
            if ( $user ) {
                if (password_verify($password, $user['password'])){
                    $token = JWT::encode($raw, ALUE_KEY);
                    $expiration = date(ALUE_DATE_FORMAT, intval($expire));

                    // Generate access token if all credentials are verified and valid
                    return $response
                        ->withHeader('Content-Type', 'application/json')
                        ->withJson(
                        array
                        (
                            "response" => array
                            (
                                "status" => 201,
                                "message" => "Created",
                                "username" => $user['username'],
                                "access_token" => $token,
                                "expiration" => $expiration,
                            )
                        ), 201
                );
                } else {
                return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(
                    array
                    (
                        "response" => array
                        (
                            "status" => 401,
                            "message" => "Unauthorized",
                            "error" => "Invalid username password"

                        )
                    ), 401
                );
                }
            } else {
                if (! $database->describe($queryTable)) {
                    $type = "Unable to fetch table name: $queryTable";
                } else {
                    $type = "Invalid username username";
                }
                return $queryTable;
            return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(
                            array
                (
                    "response" => array
                    (
                        "staus" => 400,
                        "message" => "Bad Request",
                        "error" => $type
                    )
                ), 400
            );
            }
        } // GET TOKEN

        else if ($procedure == "add") {

            if ($database->row('admin', 'username', $admin)) {
                return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(array
                    (
                        "response" => array
                        (
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => "duplicated username"
                        )
                    ), 400
                );
            }
            $data = array(
                "username" => $admin,
                "password" => password_hash($password, PASSWORD_BCRYPT),
                "user_level" => $userLevel
            );
            return Handler::method($method, $request, $response, $data, 'admin');

        } // ADD ADMIN
    } else {
        // Something went wrong unable to establish database connection
        return json_encode(
            array
            (
                "response"                  => array
                (
                    "status"                => 406,
                    "messege"               => "Not Acceptable",
                    "type"                 => "Unable to establish database connection"
                )
            ));
    }

});
