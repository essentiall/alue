<?php
use \Firebase\JWT\JWT;
use \ALUE\DB\DB;
use \ALUE\Token\Access as Access;


$app->map(['POST'],'/[{id}]', function($request, $response){

$auth = new Access;
$token = $auth->getBearerToken();
$param = $request->getParsedBody();

    try {
        $tables = array('admin', 'tbl_coach', 'tbl_trainee');
        return DB::login($tables, $param['username'], $param['password'], $response );
    } catch(Exception $e) {
        return $response
                ->withHeader("Content-Type", "application/json")
                ->withJson(array
            (
            "response" => array(
            "status" => 403,
            "message" => "Access Denied",
            "error" => $e->getMessage()
            )

        ), 403
    );
    }



});
