<?php
use \Firebase\JWT\JWT;
use \ALUE\Method\Handler;
use \ALUE\Token\Access as Access;
use \ALUE\Database\Connection as Connection;
use \Psr\Http\Message\StreamInterface;
use \ALUE\DB\DB;
use \ALUE\INI\INI as INI;


$app->group('/curriculum', function() use($app){
    $auth = new Access;
    $GLOBALS['token'] = $auth->getBearerToken();
    $GLOBALS['database'] = new Connection;
    $GLOBALS['credentials'] = array
    (
        "user" => INI::get('username'),
        'pass' => INI::get('password'),
        'db' => INI::get('database'),
        'host' => INI::get('servername')
    );

    $app->map(['GET'], '', function($request, $response){
        return $response
                ->withJson(array("response" => array(
                        "status" => 403,
                        "message" => "Access Denied",
                        "error" => "access to this resource are not allowed."
        )), 400 );
    });

    //HEADER
    $app->map(['GET', 'POST', 'PUT', 'DELETE'], '/header/[{id}]', function($request,$response,$args){
        $method = $request->getMethod();
        $param = $request->getAttribute('id');
        $data = $request->getParsedBody() ? $request->getParsedBody() : NULL;
        require "spp.class.php";
        try {
            $jwt = JWT::decode($GLOBALS['token'], ALUE_KEY, array('HS256'));

            if ($method === 'GET') { //REQUEST AND PULL DATA
                if ($param === 'get') {
                    return DB::getAllRows('tbl_curriculum_set_header', $response,'tbl_curriculum_set_header');
                    exit;
                }
                return $response
                        ->withHeader("Content-Type", "application/json")
                        ->withJson(ssp::simple($_GET, $GLOBALS['credentials'], 'tbl_curriculum_set_header', 'curriculum_id',
                    array(
                            array("db" => "curriculum_id", "dt" => 0),
                            array("db" => "curriculum_name", "dt" => 1),
                            array("db" => "total_weeks_num", "dt" => 2),
                            array("db" => "assesstment_lvl", "dt" => 3),
                            array("db" => "curriculum_id", "dt" => 4,  "formatter" => function($d, $row ) {
                                return '<a class="btn-show-view-curriculum-form" data-id="' . $d . '" href="javascript:void(0);">View</a> | <a class="btn-show-edit-form" data-id="' . $d . '" href="javascript:void(0);">Edit</a> |  <a class="btn-show-delete-form" data-id="'.$d.'" href="javascript:;">Delete</a>';
                            }),
                    )
                ), 200);
            }
            if (! $data && $method == "POST") {
                throw new Exception('required key\'s are missing');
            } else {
                if ($method === 'POST') {
                    foreach($data as $keys => $values) {
                        if($GLOBALS['database']->row('tbl_curriculum_set_header','curriculum_id', $values)){
                            throw new Exception("duplicated record $keys"."['$values']");
                            break;
                        }
                    }
                }
                return Handler::method($method, $request, $response, $data,'tbl_curriculum_set_header');
            }
        } catch(Exception $e) {
            return $response
                   ->withJson(array(
                       "response" => array(
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => $e->getMessage(),
                )
            ), 400);

        }
    });

    //DETAILS
    $app->map(['GET', 'POST', 'PUT', 'DELETE'], '/details/[{id}]', function($request,$response,$args){
        $method = $request->getMethod();
        $parameter = isset($_GET['par']) ? $_GET['par'] : "";
        $index = isset($_GET['index']) ? $_GET['index'] : "";
        $param = $request->getAttribute('id');
        $data = $request->getParsedBody() ? $request->getParsedBody() : NULL;
        $raw = $request->getBody();
        require "spp.class.php";
        try {
            $jwt = JWT::decode($GLOBALS['token'], ALUE_KEY, array('HS256'));

            if ($method === 'GET') { //REQUEST AND PULL DATA
                if ($param === 'get') {
                    return DB::getRows('tbl_curriculum_set_header', $response, $index,   $parameter );
                    exit;
                }
            }
            if ( ! $data && $method === "POST" ) {
                if( ! $raw ) {
                    throw new Exception('required key\'s are missing addasd');
                }
            } else {
                if ($parameter === 'add') {
                    $data = json_decode($raw->getContents(), true);

                    $check = DB::addCurriculum('tbl_curriculum_set_header', $data);
                    if ($check) {
                        return $response
                                ->withHeader("Content-Type", "application/json")
                                ->withJson(array(
                                    "status" => 200,
                                    "message" => "ok",
                                    "success" => "Curriculum set added successfully"
                                ), 200);
                    } else {
                        throw new Exception("Failed to add new curriculum, please check your raw data.");
                    }
                    exit;
                }
                if ($method === 'POST') {
                    foreach($data as $keys => $values) {
                        if($GLOBALS['database']->row('tbl_curriculum_set_detail','curriculum_id', $values)){
                            throw new Exception("duplicated record $keys"."['$values']");
                            break;
                        }
                    }
                }
                
                return Handler::method($method, $request, $response, $data,'tbl_curriculum_set_detail'); 
            }
        } catch(Exception $e) {
            return $response
                   ->withJson(array(
                       "response" => array(
                            "status" => 400,
                            "message" => "Bad Request",
                            "error" => $e->getMessage(),
                )
            ), 400);
        }
    });
});